# Ssimple

version 1.5.1

Stay Simple, Stay Curious.

An extremely simple theme for hexo.

## Update log

`1.5.1` 2020.4.14 optimize the style for darkmode

`1.5.0` 2020.4.13 add Codeforces user tag

`1.4.1` 2020.4.13 add style for table.

`1.4.0` 2020.4.11 add encrypt widget and label.

`1.3.3` 2020.1.27 optimize mobile device experience

`1.3.2` 2020.1.26 add style for inline code

`1.3.1` 2020.1.16 add print-friendly css.

`1.3.0` 2020.1.15 add directory page.

`1.2.0` 2020.1.14 add paginator. update index. add valine comment, page view counter and wordcount. also darkmode for valine is added.

`1.1.0` 2020.1.13 update article page. add nightmode and back to top button (use Iconfont). add previous and next posts' links.

`1.0.0` 2020.1.12 init

## Reference

https://www.ahonn.me/blog/create-a-hexo-theme-from-scratch

https://hexo.io/zh-cn/docs/variables#%E9%A1%B5%E9%9D%A2%E5%8F%98%E9%87%8F

https://www.iconfont.cn/

https://developer.mozilla.org/zh-CN/docs/Web/API/Window/localStorage
