var body=document.getElementsByTagName('body')[0];
var side_button=document.getElementsByClassName('side-button')[0];
var darkmode_button=document.getElementById('darkmode-button');
var moon='<span class="iconfont icon-moonbyueliang"></span>';
var sun='<span class="iconfont icon-sunfill"></span>';
(function(){
    if(localStorage.getItem('notes-sshwy-name-darkmode')){
        body.classList.add('darkmode')
        side_button.classList.add('darkmode-sdbtn');
        darkmode_button.innerHTML=sun;
    }
})();
function darkmode(){
    body.classList.add('darkmode')
    side_button.classList.add('darkmode-sdbtn');
    darkmode_button.innerHTML=sun;
    localStorage.setItem('notes-sshwy-name-darkmode',1);
}
function lightmode(){
    body.classList.remove('darkmode')
    side_button.classList.remove('darkmode-sdbtn');
    darkmode_button.innerHTML=moon;
    localStorage.removeItem('notes-sshwy-name-darkmode');
}
function chang_light_dark_mode(){
    if(darkmode_button.innerHTML==sun)lightmode();
    else darkmode();
}
function back_to_top(){
    document.documentElement.scrollTop=0
}
function go_to_comment(){
    document.documentElement.scrollTop=document.getElementById('vcomments').offsetTop;
}
