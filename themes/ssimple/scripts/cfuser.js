/* global hexo */

'use strict';

function codeforcesUser(args) {
    args = args.join(' ').split('@');
    var classes = args[0] || 'unr';
    var text    = args[1] || '';

    !text && hexo.log.warn('Codeforces username must be defined!');
    classes = classes.trim().toLowerCase();
    switch(classes){
        case "lgm":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong class="lgm-head">${text[0]}</strong><strong class="lgm-tail">${text.substring(1)}</strong></span></a>`;
        case "igm":
        case "gm":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong>${text}</strong></span></a>`;
        case "im":
        case "m":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong>${text}</strong></span></a>`;
        case "cm":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong>${text}</strong></span></a>`;
        case "e":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong>${text}</strong></span></a>`;
        case "s":
            return `<a href="https://codeforces.com/profile/${text}" target="_blank" style="text-decoration: none;"><span class="codeforces ${classes}"><strong>${text}</strong></span></a>`;
        default:
            return text;
    }
}

hexo.extend.tag.register('codeforces', codeforcesUser, {ends: false});
