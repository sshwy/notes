hexo.extend.generator.register('directory', function(locals){
    let map=new Map();
    let map_posts=new Map();
    let map_dirs=new Map();
    let map_pardirs=new Map();
    locals.posts.forEach(function(post,id){//按路径给文章分类
        map.set(id,post);
        var path=post.source.split('/');
        //console.log(path);
        var curpath='',laspath='';
        for(var i=0;i<path.length-1;i++){
            curpath+=(i==0?'directory':path[i])+'/'
            if(map_posts.get(curpath)==undefined){//init
                map_posts.set(curpath,[]);
                map_dirs.set(curpath,[]);
                map_pardirs.set(curpath,laspath);
            }
            if(i==path.length-2){
                map_posts.set(curpath,map_posts.get(curpath).concat(id));
            }else if(map_dirs.get(curpath).indexOf(path[i+1])==-1){
                map_dirs.set(curpath,map_dirs.get(curpath).concat(path[i+1]));
            }
            laspath=curpath;
        }
    });
    //console.log(map_pardirs);
    var results=[];
    var a=map_posts.entries(),b=map_dirs.entries(),c=map_pardirs.entries();
    var cura=a.next(),curb=b.next(),curc=c.next();
    while(!cura.done){
        results=results.concat((function(path,posts_id,dirs,par_path){
            //console.log('path='+path);
            var posts=[]
            posts_id.forEach(function(id){posts=posts.concat(map.get(id))});

            return {
                path: path ,
                data: {
                    posts: posts,
                    dirs: dirs,
                    dir: path.replace(par_path,'').replace('/',''),
                    dir_path: path,
                    par_path: par_path
                },
                layout: 'directory'
            }//对应到模板里是page变量
        })(cura.value[0],cura.value[1],curb.value[1],curc.value[1]));

        cura=a.next(),curb=b.next(),curc=c.next();
    }
    return results
});
//逻辑：这里gen出的变量放到对应的模板上去渲染
