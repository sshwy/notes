---
title: IOI2020 国家集训队作业小结 5
date: 2019-12-10 08:49:15
tags:
---

## [AGC030 D Inversion Sum](https://atcoder.jp/contests/agc030/tasks/agc030_d)

设 $f(x,i,j)$ 表示在第 $x$ 次操作后，$A_i$ 大于 $A_j$ 的概率（即第 $i$ 个位置大于第 $j$ 个位置的概率）。每次我们有一半的概率执行这个操作。

转移的时候只修改 $(i,j),(i,k),(j,k),(k,i),(k,j),k\in[1,n]$ 的 dp 值即可，因此不需要第一维，转移的复杂度是 $O(n)$ 的。

总复杂度 $O(n^2+nq)$。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc030_d.cpp)

## [AGC031 D A Sequence of Permutations](https://atcoder.jp/contests/agc031/tasks/agc031_d)

首先，我们定义两个置换的乘法运算：
$$
pq=\{p_{q_i}\}
$$
那么可以发现 $f(p,q)=qp^{-1}$。

另外，$(abc)^{-1}=c^{-1}b^{-1}a^{-1}$。置换的乘法是有结合律的。

![agc-part-5-1](../../images/agc-part-5-1.png)

这里是 $abcd$ 的乘法运算过程。结果为 $(3,2,1,4)$。

那么来找一些规律吧：
$$
\begin{aligned}
a_1&=p\\
a_2&=q\\
a_3&=qp^{-1}\\
a_4&=qp^{-1}q^{-1}\\
a_5&=qp^{-1}q^{-1}pq^{-1}
\end{aligned}
$$
相信聪明的你已经找到规律了。我们把 $a_i$ 写成 $A_iB_iA_i^{-1}$ 的形式。那么
$$
\begin{aligned}
(A_1,B_1)&=(\varepsilon,p)\\
(A_2,B_2)&=(\varepsilon,q)\\
(A_3,B_3)&=(\varepsilon,qp^{-1})\\
(A_4,B_4)&=(q,p^{-1})\\
(A_5,B_5)&=(qp^{-1},q^{-1})\\
(A_6,B_6)&=(qp^{-1},q^{-1}p)\\
(A_7,B_7)&=(qp^{-1}q^{-1}p,p)\\
(A_8,B_8)&=(qp^{-1}q^{-1}p,q)
\end{aligned}
$$
所以有一个 6 的循环。一个 $(A,p)$ 在过 6 轮后会变成 $(Aqp^{-1}q^{-1}p,p)$。第一维转化为置换的幂，然后就可以 $O(n)$ 做了。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc031_d.cpp)

## [AGC030 E Less than 3](https://atcoder.jp/contests/agc030/tasks/agc030_e)

我们考虑在 10 中间放一个蓝线，01 中间放一个红线。那么我们可以把变化过程描述为线的移动过程：

![image-20191210123413610](../../images/agc-part-5-3.png)

当然，移动的时候有一些规则限制。但是不难证明，初始局面一定能通过合理的移动方式来得到最终局面，移动的距离是每条线移动距离的绝对值之和：

![image-20191210123303542](../../images/agc-part-5-2.png)

那么我们枚举第一条线和哪条匹配，然后 $O(n)$ 计算总代价即可。总时间复杂度 $O(n^2)$。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc030_e.cpp)

## [AGC030 F Permutation and Minimum](https://atcoder.jp/contests/agc030/tasks/agc030_f)

首先考虑分类，我们将 $(A_{2i-1},A_{2i})$ 可以分三类：$(-1,-1)$、$(-1,x),(x,-1)$ 和 $(x,y)$。

对于第三类可以直接不管。对于第二类，我们可以把 $(x,-1)$ 统一为 $(-1,x)$，这样不影响答案。

对于第一类，设集合 $S$ 表示满足 $(A_{2i-1},A_{2i})=(-1,-1)$ 的 $i$ 的集合。那么我们强制所有 $(-1,-1)$ 在填完后的 $\min$ 是单减的。算出这个方案数后乘上 $|S|!$ 就是答案。

我们考虑 DP。考虑按照值域从大到小 DP，这样我们当前的数总是最小的。

设 $f(i,j,k)$ 表示填了权值 $[i,2n]$ 的数，目前剩下 $j$ 个天生的 $(-1,x)$，$k$ 个后天的 $(-1,x)$ 的方案数。天生就是第二类，后天就是第三类填了一个数上去。

考虑数值 $i$ 所在的位置的二元组 $(A_{2x-1},A_{2x})$：

1. 第三类：那么这个 $i$ 就是混子，$f(i,j,k)\gets f(i+1,j,k)$。
2. 第二类：那么这个 $i$ 可以选择新加一个天生的 $(-1,x)$ 上去（因为它本来就是天生的），也可以选择与某个后天的 $(-1,x)$ 合并（相当于你把 $x$ 放在 $(-1,i)$ 上）。但是它不能选择新加一个后天的 $(-1,i)$。因为它的命运是已经被安排的。
3. 第一类：它的选择最多。它可以新加一个后天的 $(-1,x)$，或者合并一个后天的 $(-1,x)$，或者合并一个先天的 $(-1,x)$。但是它不能新加一个先天的 $(-1,x)$。因为它不是第二类。

注意，在合并后天的 $(-1,x)$ 的时候我们不考虑它选择的是哪一个 $(-1,x)$。因为我们强制了 $(-1,-1)$ 是填完后是单减的，因此它理所当然选择最考前的那个后天的 $(-1,x)$。但是在合并先天的 $(-1,x)$ 的时候我们就要考虑它的选择了，乘上一个 $j$ 的系数即可。

也可以用括号序列计数的方式理解这个 DP，相当于左右括号的匹配过程。时间复杂度 $O(n^3)$。

[更](https://www.codeleading.com/article/40061882397/) [多](http://www.ishenping.com/ArtInfo/2824304.html) [题](https://jhdjames37.github.io/2018/12/31/Goodbye-2018-AGC030/) [解](https://www.cnblogs.com/zhouzhendong/p/AGC030F.html)

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc030_f.cpp)

## [AGC031 E Snuke the Phantom Thief](https://atcoder.jp/contests/agc031/tasks/agc031_e)

考虑枚举拿到的宝石数为 $k$。假设这些宝石的横坐标排序后为 $x_1,x_2\cdots,x_k$。对于一个限制`L a b`，相当于 $x_{b+1}\ge a+1$；对于`R a b`，相当于 $x_{k-b}\le a-1$。纵坐标序列同理。

那么我们就可以预处理出 $k$ 个位置的横纵坐标的取值范围。这样就可以建模了。

1. 源点连向 $k$ 个 $x$ 限制；
2. 每个 $x$ 限制向可行的物品连边；
3. 每个物品拆点为边；
4. 物品连向可行的 $y$ 限制；
5. $k$ 个 $y$ 限制连向汇点。

相当于把横纵坐标的限制分开处理。合理设置容量和费用后计算费用流即可。

时间复杂度 $O(n^4)$。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc031_e.cpp)

## [AGC031 F Walk on Graph](https://atcoder.jp/contests/agc031/tasks/agc031_f)

考虑倒着走。相当于我们 $(u,x)$ 可以通过 $(u,v,c)$ 转移到 $(v,2x+c)$。问 $(s,0)$ 与 $(t,r)$ 是否连通。

考虑到 $(u,x)\to (v,2x+c)\to (u,4x+3c)\to (v,8x+7c)\to \cdots$，发现 $u,v$ 的状态是一个双射。即 $(u,x)\leftrightarrow (v,2x+c)$。那么就转化为了无向图。

然后考虑到 $(u,v,a),(u,w,b)$，我们可以交替走两条边使得 $(u,4x+3b)\leftrightarrow (u,4x+3a)$。因此有 $(u,x)\leftrightarrow (u,x+3(a-b))$。

那么我们把两两边权差取一个 gcd，设为 $g$。那么显然 $(u,x)\leftrightarrow (u,x+3g)$。

咕咕咕

