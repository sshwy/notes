---
title: 保密
date: 2020-04-16 15:11:15
tags:
---

{% enc boboniuandjianghu 高爸题的标题（全小写无空格） %}

实际上的标题是：

# Boboniu Round 花絮

## 前前期准备

要准备 Codeforces 比赛，首先要有一些题目的 idea。于是在 3 月份的时候我决定，在我的 OI 生涯中留下一些东西。因此我找到了高爸（{% codeforces GM @ZZZZZZZZZZZZZZZZZZ %}）、徐队（{% codeforces M @xryjr233 %}）、杜以老师（{% codeforces M @dysyn1314 %}）和董先生（{% codeforces M @jhdonghj112 %}）来一起准备这场比赛。在此致谢。

起初是大家各自贡献了 Idea。我先把 Space Travel 给搬过来了，这是我之前出的一个 PJT4。徐队在搞一个基于 NOIoT1 的题，顺便 YY 了一下高爸的题的题解。杜以老师搞了一个背景很现实的 D2A。董先生搞了一个海星的 D2B。我把徐队的题改成了交互。这样就有了 5 题了。

但是我的野心有点不满足。既然要搞就搞大的！于是我把宝藏题（Infinite Treasure）拿出来了。是一个尘封的 Idea。再加上基于当时省选模拟赛上的一个算法编出的仙人掌题，似乎可以 7 题搞 Div1 了。配置大概是这样的：

| 难度 | 题目名称                            | Idea                                                       |
| ---- | ----------------------------------- | ---------------------------------------------------------- |
| D2A  | Boboniu and His School Days         | {% codeforces M @dysyn1314 %}                              |
| D2B  | Boboniu and Calligraphy Competition | {% codeforces M @jhdonghj112 %}                            |
| D1A  | Boboniu and Infinite Treasure       | {% codeforces M @sshwyR %}                                 |
| D1B  | Boboniu and Staff Assessment        | {% codeforces M @xryjr233 %}（{% codeforces M @sshwyR %}） |
| D1C  | Boboniu and Jianghu                 | {% codeforces GM @ZZZZZZZZZZZZZZZZZZ %}                    |
| D1D  | Boboniu and Space Travel            | {% codeforces M @sshwyR %}                                 |
| D1E  | Boboniu and Planetary System        | {% codeforces M @sshwyR %}                                 |

然后我就交了 7 题的 Proposal 上去（在 Mar12 的时候）。

前期不太懂 Codeforces 规则，于是就边想 Idea 边在 Polygon 上造题。因此这 7 题都造出来了。我们几个还互测了。

后来我 YY 了折叠题。这其实是算一个尘封的 Idea，因为我以前似乎是想到过的，但当时水平太差就认为这题不可做。然后我花了一个下午的时间想这题，想到了三折必折的部分以及后面的一些处理方式，感觉很妙，于是就和高爸讨论。讨论过程中发现我没有处理不含三折的情况，然后高爸点破，不含三折的只有根号次。于是我花了一些时间证明了一下，边写中文题解边就把这题做出来了。当然，中途也请教过杜老师（{% codeforces LGM @MiFaFaOvO %}），他说难度是 3000 左右。

然后去写这题的 std，越写越觉得妙。代码不长，但思维很深。这是我自己造出的很有成就感的题，也是我目前造出的最难的题。

差不多在这个时候吧，我联系了一下东哥（{% codeforces M @Xiejiadong %}）来审题。虽然他只审了前三题就咕了，但还是提出了有关题面的意见，以及教会我使用 mashup 开 tester round。然后我们也很开心地一起打过几场比赛。我也和他讨论了一下折叠题。

由于当时不懂规则，于是东哥就怂恿我找{% codeforces LGM @300iq %}对线。于是我先和 IQ 哥在 Codeforces 的 talk 上聊（其实是我单方发了一个超长信息），然后他就接收了我的 QQ 好友申请。然后和他尬聊了两三句，也没问出个啥，就知道了可能不只等两周（即 CF 很鸽）的信息。

## 前期准备

后来打了一场 Div2，发现出题人是 Chinese，于是就给他发 talk 询问经验。结果大家聊天甚欢，就加了 QQ。他就是{% codeforces M @triple__a %}。

和他交流之后就懂了很多内幕，顺便也邀请他当了 tester。他也审过了我们的题，觉得徐队的交互题不 interesting，于是就魔改了一下。他也提供了一个 D1A 的 Idea。这下我们总共就有 10 个 Idea 了。他也直接秒了 Space Travel，问我为啥把 Space Travel 放高爸题后面。我想了想觉得有理。但是 Space Travel 不是很有趣。因此我也去想了一个新的 Idea。

由于是在家隔离，反正没事做，就也在 Polygon 上把题造出来了。

然后就有了三角网格题（Boboniu String）。本来是三角网格的背景题，结果我套了一个字符串的壳上去。这题也很有意思。我写的二分，徐队和高爸写的是线性做法。结果大家开场就 WA。后来他们过了后，我加强了一下数据一测，结果把高爸的代码卡掉了。他当时只 WA 了一个点。当时的我：非常感谢高爸提供了一个高质量的错解！

不过这也没完。我写了一个爬山，结果卡不掉。只能无奈把它加到正解中。

在这期间，由于东哥给了我 CF 的 Polygon Advices，于是我顺便把它翻译成了中文。

又叫了杜老师，这次是让他 test 这 11 个题。结果咕咕了……

终于在 Apr12 的时候，Mike 回复了，指定了 IQ 哥来 review。OHHHHHH！现在的配置是

| 难度    | 题目名称                             | Idea                                                         |
| ------- | ------------------------------------ | ------------------------------------------------------------ |
| D2A     | Boboniu and His School Days          | {% codeforces M @dysyn1314 %}                                |
| D2B     | Boboniu and Calligraphy Competition  | {% codeforces M @jhdonghj112 %}                              |
| D1A     | Boboniu and Infinite Treasure        | {% codeforces M @sshwyR %}                                   |
| D1A     | Boboniu and Birthday Gift            | {% codeforces M @triple__a %}                                |
| D1B     | Boboniu String                       | {% codeforces M @sshwyR %}                                   |
| D1B     | Boboniu and Staff Assessment         | {% codeforces M @xryjr233 %}（{% codeforces M @sshwyR %}）   |
| D1B-D1C | Boboniu and Another Staff Assessment | {% codeforces M @triple__a %}（基于 Boboniu and Staff Assessment） |
| D1C     | Boboniu and Space Travel             | {% codeforces M @sshwyR %}                                   |
| D1D     | Boboniu and Jianghu                  | {% codeforces GM @ZZZZZZZZZZZZZZZZZZ %}                      |
| D1E     | Boboniu and Planetary System         | {% codeforces M @sshwyR %}                                   |
| D1F     | Boboniu and Banknote Collection      | {% codeforces M @sshwyR %}                                   |

IQ 哥在过了三四天后（Apr16），对一些题做了回复：

| 难度 | 题目名称                            | 评价                       |
| ---- | ----------------------------------- | -------------------------- |
| D2A  | Boboniu and His School Days         | 对于D2A而言过难。          |
| D2B  | Boboniu and Calligraphy Competition | 有重题。                   |
| D1A  | Boboniu and Infinite Treasure       | 有点标准化，但可以采用。   |
| D1A  | Boboniu and Birthday Gift           | 不错的想法，可以采用。     |
| D1B  | Boboniu String                      | 对D1B而言过难。            |
| D1C  | Boboniu and Space Travel            | 无趣，没有技巧性。不通过。 |

因此经过了一些商讨，D2A和D2B大概要回炉了。当然杜以老师向IQ哥提出了D2A的建议（毕竟是自己的题），让他读一读legend。至于Space Travel被ban我心里有数，因此回复评论表赞同。

{% endenc %}