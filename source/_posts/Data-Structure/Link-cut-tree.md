---
title: LCT 初步
date: 2019-09-20 12:03:55
tags:
  - Notes
---

LCT（Link Cut Tree），可以动态维护树的结构变化以及路径信息。

## 对 LCT 的理解

LCT 可以理解为 Splay 维护树链剖分。Splay 指伸展树，是一种自适应的平衡树；树链剖分则是指广义地将树剖分为若干个链，而非狭义的轻重链剖分。

总体来说，LCT 可以解决以下的问题：
1. 连接两个点；
2. 删除某条边；
3. 维护可合并的路径信息。

具体来说，Splay 按照树链从深到浅的顺序维护树链的结点。而 LCT 就是一个 Splay 森林。我们一般使用父节点和子节点指针可以维护 Splay 的特征。因此，如果某个指针它不符合 Splay 该有的特征（比如根结点的父节点指针是无意义的），那么这类指针就被用于维护树链之间的信息。

简单地说，LCT 的父节点指针在 Splay 内就是 Splay 的父节点指针，在 Splay 外（根节点的父节点指针）表示的就是这条树链的链顶的父节点指针。

## 通用函数

你需要实现以下函数：

```cpp
int new_node(int v);
void pushup(int u);
void noderv(int u);
void pushdown(int u);
```

1. 新建一个权值为 v 的结点，返回结点编号；
2. 上传结点信息；
3. 给一个结点打翻转标记；
4. 标记下传（主要是翻转标记）。

## 核心函数

```cpp
bool isroot(int u);
bool get(int u);
void rotate(int u);
void splay(int u);
void access(int u);
void makeroot(int u);
```

1. 判断 u 是不是所在 Splay 的根结点；
2. 判断 u 是所在 Splay 中左儿子还是右儿子的身份；
3. Splay 的双旋，需要用到 isroot；
4. 把 u 旋转到所在 Splay 的根结点；
5. 把 u 到原树根结点的路径变成一条树链（不包括它之前树链的部分）
6. 把 u 变成原树的根。

## 常用函数

```cpp
bool check_link(int x,int y);
void link(int x,int y);
bool check_edge(int x,int y);
void cut(int x,int y);
```

1. 检查 x,y 是否连通；
2. 在不连通的 x,y 之间连一条边；
3. 检查 (x,y) 的边是否存在；
4. 删除 (x,y) 这条边（保证存在）

在某些题目条件下不用写 1 和 3。

## 模板

```cpp
/******************heading******************/
const int SZ=1e6+6;

int tot,ch[SZ][2],f[SZ],val[SZ],sz[SZ],rv[SZ];
int sxor[SZ];

int new_node(int v){
    ++tot,ch[tot][0]=ch[tot][1]=f[tot]=rv[tot]=0;
    sz[tot]=1,val[tot]=v,sxor[tot]=v;
    return tot;
}
void pushup(int u){
    sz[u]=sz[ch[u][0]]+sz[ch[u][1]]+1;
    sxor[u]=sxor[ch[u][0]]^sxor[ch[u][1]]^val[u];
}
void noderv(int u){ if(u)rv[u]^=1; }
void nodeassign(int u,int v){ val[u]=v, pushup(u); }
void pushdown(int u){
    if(rv[u])swap(ch[u][0],ch[u][1]),
        noderv(ch[u][0]),noderv(ch[u][1]),rv[u]=0;
}
bool isroot(int u){return ch[f[u]][0]!=u&&ch[f[u]][1]!=u;}
bool get(int u){return ch[f[u]][1]==u;}
void print(){
    puts("---------------------------------");
    FOR(u,1,5)printf("u=%2d,lc=%2d,rc=%2d,sz=%2d,f=%2d,rv=%2d\n",
            u,ch[u][0],ch[u][1],sz[u],f[u],rv[u]);
    puts("---------------------------------");
}
void rotate(int u){
    int p=f[u],pp=f[p],k;
    pushdown(p),pushdown(u),k=get(u);//k 的赋值必须在 pushdown 后！
    if(!isroot(p))ch[pp][get(p)]=u;//!!!
    ch[p][k]=ch[u][!k], f[ch[u][!k]]=p;
    ch[u][!k]=p,f[p]=u, f[u]=pp;
    pushup(p),pushup(u);
}
void splay(int u){
    pushdown(u);
    for(int p;p=f[u],!isroot(u);rotate(u))
        if(!isroot(p))rotate(get(p)==get(u)?p:u);
}
void access(int u){
    for(int p=0;u;p=u,u=f[u])splay(u),ch[u][1]=p,pushup(u);
}
void makeroot(int u){ access(u), splay(u), noderv(u); }
bool check_link(int x,int y){
    makeroot(x),access(y),splay(x),splay(y);
    return !(isroot(x)&&isroot(y));
}
void link(int x,int y){ makeroot(x), f[x]=y; }
bool check_edge(int x,int y){
    if(!check_link(x,y))return 0;
    makeroot(x),access(y),splay(y);
    if(ch[y][0]!=x||ch[x][1])return 0;
    return 1;
}
void cut(int x,int y){
    makeroot(x),access(y),splay(y),ch[y][0]=f[x]=0,pushup(y);
}
void assign(int x,int y){ splay(x), nodeassign(x,y); }
int query(int x,int y){ return makeroot(x), access(y),splay(y),sxor[y]; }
/*
 * 模板：Luogu3690
 * new_node: 新建权值为 v 的结点
 * pushup: 信息更新
 * pushdown: 标记下传，主要是翻转标记
 * noderv: 对某一个结点施加标记。
 *     LCT 的标记不同于线段树，必须在下传的时候再更新当前结点的信息。不然
 *     get 的时候会出锅
 * nodeassign: 模板题需要
 * isroot: 是否是所在 Splay 的根
 * get: 是 Splay 上左儿子还是右儿子
 * print: 调试函数
 * rotate: 双旋，注意与 Splay 的双旋不同，要判 f[u] 是不是 root，不然 f[f[u]] 的
 *     儿子不能乱赋值
 * splay: 把当前结点旋转到当前 Splay 的根结点，要用到 isroot 函数。一开始
 *     先 pushdown。
 * access: 把当前结点到根的路径连成一个 Splay，注意这个 Splay 只包含当前结点
 *     到根这段路径上的点，不包括当前结点子树的那一段（非到叶结点的树链）
 *     access 完之后这个点不一定是所在 splay 的根，需要手动 splay 一下
 * makeroot: 把当前结点变成原树的根，这个结点也会顺便变成所在 Splay 的根。
 * check_link: 判断两个点是否连通。
 * link: 连接两个不连通的点
 * check_edge: 判断两个点是否直连通（有没有边）
 * cut: 删掉 (x,y) 的边。
 * assign: 模板题需要
 * query: 模板题需要
 */
```

## BZOJ 2631

> 维护Link/Cut，链加/乘/求和

对于Tag的维护，除了翻转Tag是在修改的时候打标记，下传的时候更新当前结点状态；其他标记都是在修改的时候打标记顺便更新该结点信息，在下传的时候更新子节点信息（与线段树相同）

```cpp
#include<algorithm>/*{{{*/
#include<cctype>
#include<cassert>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<map>
#include<queue>
#include<set>
#include<vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int,int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i,a,b) for(int i=(a);i<=(b);++i)
#define ROF(i,a,b) for(int i=(a);i>=(b);--i)
namespace RA{
    int r(int p){return 1ll*rand()*rand()%p;}
    int r(int L,int R){return r(R-L+1)+L;}
}
namespace IO{
    char nc(){ static char bf[100000],*p1=bf,*p2=bf; return p1==p2&&(p2=(p1=bf)+ fread(bf,1,100000,stdin),p1==p2)?EOF:*p1++; }
    int rd(){ int res=0; char c=getchar(); while(!isdigit(c))c=getchar(); while(isdigit(c))res=res*10+c-'0',c=getchar(); return res; }
}/*}}}*/
/******************heading******************/
const int SZ=1e5+50,P=51061;
int n,q;

int tot,ch[SZ][2],f[SZ],rv[SZ];
int val[SZ],s[SZ],sz[SZ],tadd[SZ],tmul[SZ];

void print(){
    puts("-------------------------------------");
    FOR(u,1,n)
        printf("u=%d,lc=%d,rc=%d,f=%d,sz=%d, s=%d,tmul=%d,tadd=%d\n",
                u,ch[u][0],ch[u][1],f[u],sz[u],s[u],tmul[u],tadd[u]);
    puts("-------------------------------------");
}
int newnode(int v){
    ++tot,ch[tot][0]=ch[tot][1]=f[tot]=rv[tot]=0;
    val[tot]=s[tot]=v,tmul[tot]=1,tadd[tot]=0,sz[tot]=1;
    return tot;
}
void pushup(int u){
    s[u]=(s[ch[u][0]]+s[ch[u][1]]+val[u])%P;
    sz[u]=sz[ch[u][0]]+sz[ch[u][1]]+1;
}
void nodeadd(int u,int v){
    s[u]=(s[u]+1ll*sz[u]*v)%P,(val[u]+=v)%=P,(tadd[u]+=v)%=P;
}
void nodemul(int u,int v){
    s[u]=1ll*s[u]*v%P,val[u]=1ll*val[u]*v%P,
        tmul[u]=1ll*tmul[u]*v%P,tadd[u]=1ll*tadd[u]*v%P;
}
void pushdown(int u){
    if(rv[u])
        swap(ch[u][0],ch[u][1]),
        rv[ch[u][0]]^=1,rv[ch[u][1]]^=1,rv[u]=0;
    if(tmul[u]!=1)
        nodemul(ch[u][0],tmul[u]),
        nodemul(ch[u][1],tmul[u]),
        tmul[u]=1;
    if(tadd[u])
        nodeadd(ch[u][0],tadd[u]),
        nodeadd(ch[u][1],tadd[u]),
        tadd[u]=0;
}
bool isroot(int u){ return ch[f[u]][0]!=u&&ch[f[u]][1]!=u; }
bool get(int u){ return ch[f[u]][1]==u; }
void rotate(int u){
    int y=f[u],z=f[y],k;
    pushdown(y),pushdown(u),k=get(u);
    if(!isroot(y))ch[z][get(y)]=u;
    ch[y][k]=ch[u][!k],f[ch[u][!k]]=y;
    ch[u][!k]=y,f[y]=u,f[u]=z;
    pushup(y),pushup(u);
}
void splay(int u){
    pushdown(u);
    for(int p;p=f[u],!isroot(u);rotate(u))
        if(!isroot(p))rotate(get(p)==get(u)?p:u);
}
void access(int u){
    for(int p=0;u;p=u,u=f[u])splay(u),ch[u][1]=p,pushup(u);
}
void makeroot(int u){
    access(u);
    splay(u);
    rv[u]^=1;
}
void link(int u,int v){
    makeroot(u), f[u]=v;
}
void cut(int u,int v){
    makeroot(u),access(v),splay(v),ch[v][0]=f[u]=0,pushup(v);
}
void add(int u,int v,int c){
    makeroot(u),access(v),splay(v);
    nodeadd(v,c);
}
void mul(int u,int v,int c){
    makeroot(u),access(v),splay(v);
    nodemul(v,c);
}
int sum(int u,int v){
    makeroot(u),access(v),splay(v);
    return s[v];
}


int main(){
    scanf("%d%d",&n,&q);
    FOR(i,1,n)newnode(1);//index 1 to n
    FOR(i,1,n-1){
        int u,v;
        scanf("%d%d",&u,&v);
        link(u,v);
    }
    FOR(i,1,q){
        char op[5];
        int u,v,x,y,c;
        scanf("%s",op);
        if(op[0]=='+'){
            scanf("%d%d%d",&u,&v,&c);
            add(u,v,c);
        }else if(op[0]=='-'){
            scanf("%d%d%d%d",&u,&v,&x,&y);
            cut(u,v),link(x,y);
        }else if(op[0]=='*'){
            scanf("%d%d%d",&u,&v,&c);
            mul(u,v,c);
        }else{
            scanf("%d%d",&u,&v);
            printf("%d\n",sum(u,v));
        }
    }
    return 0;
}
```
