---
title: 构造题选讲
date: 2020-03-25 17:45:15
tags:
---

## [ARC103 D Robot Arms](https://atcoder.jp/contests/arc103/tasks/arc103_b)

> 规定一个机械臂是$m$段臂，第$i$段长度为$d_i$。其中一端连在$(0,0)$的位置。机械臂只允许平行于坐标轴（上下左右）放置。
>
> 二维平面给你$n$个点$(x_i,y_i)$。要求你构造一个$m\le 40$的机械臂（长度自定），使得机械臂的**另一端**可以通过恰当的放置来分别落在这$n$个点上（$n$个方案）。要求给出构造方案，以及落在这$n$个点上的方案。
>
> $|x_i|,|y_i|\le 10^9$。

如果$(x_i+y_i)$的奇偶性都一样，才可能有解。

容易想到二进制构造。设$d_i=2^{m-i}$，则可以归纳法证明，满足$|x_i|+|y_i|<2^m$的点都可以落到。

如果$(x_i+y_2)\bmod 2=0$，需要在开头加一段长度为$1$的臂。

至于落在一个点上的方案，可以递归构造。

时间复杂度很小。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/arc103_b.cpp)

## [CF1227 G Not Same](https://codeforces.com/contest/1227/problem/G)

> 大小为 $n$ 的序列 $a_1, a_2,\cdots, a_n$，满足 $1 \le a_i \le n$。每次你可以选择一个子集的元素并且把这个子集的元素都$-1$，你需要在至多 $n + 1$ 次操作之内把所有数变成 $0$。每次选择的子集不能相同。
>
> $1 \le n \le 1000$。

按降序排序，考虑这样构造：

![image-20200325135405524](../images/constructive-1.png)

复杂度$O(n^2)$。

## [CF1157 G Inverse of Rows and Columns](https://codeforces.com/contest/1157/problem/G)

> 给定一个 $n \times m$ 的 $01$ 矩阵$A$，你可以选择任意行翻转、任意列翻转。问能否使得$A_{11},A_{12},\cdots,A_{1m},A_{21},\cdots,A_{2m},\cdots,A_{n1},\cdots,A_{nm}$变成非降序列。
>
> $1\le n,m\le 200$。

特判$n=1$的情况。

容易发现，我们最终的方案，要么第一行全0，要么最后一行全1。而这样就可以确定列的选择。然后再判一下选择行是否有解即可。

时间复杂度$O(nm)$。

[代码](https://gitee.com/sshwy/code/raw/master/codeforces/1157g.cpp)

## [CF1311 E Construct the Binary Tree](https://codeforces.com/contest/1311/problem/E)

> 给定 $n,d$，构造一个 $n$ 个点的二叉树，满足所有节点的深度之和等于 $d$。$1000$ 组询问。
>
> $n,d\le 5000$。

先考虑构造一棵完全二叉树。然后我们每次让总深度加1。

方法很简单。固定一条最长的链。 每次选择一个深度最深不在链上的点，把它往下移一个位置，或者接在链上在往下移一个位置即可。合理安排顺序以避免3叉的情况。

时间复杂度$O(nT)$。

[代码](https://gitee.com/sshwy/code/raw/master/codeforces/1311e.cpp)

## [AGC035 C Skolem XOR Tree](https://atcoder.jp/contests/agc035/tasks/agc035_c)

> 给定 $n$，构造一棵 $2n$ 个节点的树，其中点权 $v_i = v_{n+i} = i$，要求 $i$ 和 $i + n$ 路径上所有点权的异或和恰好是 $i$（包含端点）。
>
> $1 \le n \le 10^5$。

如果$n=2^k$则无解。

考虑到$2k\oplus(2k+1)=1$，因此我们想办法围着$1$构造菊花。对于$k(1\le 2k<n)$，我们构造
$$
{\color{blue}2k}-{\color{red}2k+1}-1-{\color{blue}2k}-{\color{red}2k+1}
$$
的链。这样$2k$和$2k+1$的条件就同时满足了。

对于$1$的条件，则构造${\color{red}1}-2-3-{\color{blue}1}$即可。

这样对于$n$的奇数的情况是适用的。对于$n$是偶数的情况，我们要再加一对$n$的点进去。

那么我们找到$1\le x,y<n,x\oplus 1\oplus y=n$的两个数$x,y$（可以证明一定存在），然后构造${\color{red}n}-x-1-y-{\color{blue}n}$ 即可。

时间复杂度$O(n)$。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc035_c.cpp)