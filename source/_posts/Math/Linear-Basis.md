---
title: 线性基学习笔记
date: 2019-10-14 20:31:06
tags:
  - Math
  - Notes
---

基（basis）是线性代数中的一个概念，它是描述、刻画向量空间的基本工具。而在现行的 OI 题目中，通常在利用基在**异或空间**中的一些特殊性质来解决题目，而这一类题目所涉及的知识点被称作「线性基」。

有关向量空间（Vector Space）、线性无关（linearly independent）、线性组合（linear combination）、张成（Span）、基（Basis）的概念就不讲了，请自行了解。

## 线性基

简单来说，线性基维护的是异或空间下的若干个线性无关向量，它们组成一个矩阵。在实现时我们使用 $b_i$ 表示最高位为第 $i$ 位且为 $1$ 的向量的值。

线性基可以使用上三角矩阵维护，插入一个向量时需要消元，复杂度 $O(\log_2n)$。在特殊题目的要求下可能需要消元成对角矩阵，这时的插入复杂度就是 $O(\log_2^2n)$。

线性基的合并就是暴力插入的过程，复杂度 $O(n\log_2^2n)$。

## HDU3949 Xor

> 求去重后异或第 $k$ 小。

消成对角矩阵的线性基可以解决异或第 K 小的问题。由于是模板题所以给出核心代码：

```cpp
/******************heading******************/

int n;

long long b[70],lb;
long long p[70],lp;
bool insert(long long x){
    ROF(i,63,0){
        if(!(x>>i&1))continue;
        if(!b[i]){
            b[i]=x,lb++;
            ROF(j,i-1,0) if(b[i]>>j&1)b[i]^=b[j];
            FOR(j,i+1,63) if(b[j]>>i&1)b[j]^=b[i];
            return 1;
        }
        x^=b[i];
    }
    return 0;
}
void print(){
    int len=0;
    FOR(i,0,63)if(b[i])len=i;
    FOR(i,0,len){
        ROF(j,len,0) printf("%lld",b[i]>>j&1);
        puts("");
    }
}
void work(){
    FOR(i,0,63) if(b[i])p[lp++]=b[i];
}
void clear(){
    memset(b,0,sizeof(b));
    lb=0,lp=0;
}
long long query(long long x){
    long long res=0;
    FOR(i,0,63)if(x>>i&1)res^=p[i];
    return res;
}
void go(){
    cin>>n;
    clear();
    bool fl=0;
    FOR(i,1,n){
        long long x;
        cin>>x;
        if(!insert(x))fl=1;
    }
    work();
    long long lim=(1ll<<lb)-1;
    int q;
    scanf("%d",&q);
    FOR(i,1,q){
        long long x;
        cin>>x;
        if(fl)x--;
        if(x>lim)cout<<-1<<endl;
        else cout<<query(x)<<endl;
    }
}
int main(){
    int t;
    cin>>t;
    FOR(i,1,t)cout<< "Case #" << i<< ":" <<endl,go();
    return 0;
}
```

## 平方数的套路

有时要求一段区间选若干个数乘积的平方数。记录每一个数的质因数次数的奇偶性，即一个 01 向量，就转化为异或和为 0 的问题。如果他们线性相关就有解，否则无解。

## 带删除的线性基

离线的话可以线段树时间分治。在线的详见 [这里](https://blog.csdn.net/a_forever_dream/article/details/83654397)

例题：最大 xor 路径。

## 线性基的一个神奇应用

[BZOJ4184 shallot](http://darkbzoj.tk/problem/4184)

通过随机赋权，判断是否线性相关的方式来判图是否连通。
