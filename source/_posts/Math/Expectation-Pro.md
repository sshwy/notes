---
title: 概率期望高级入门
date: 2019-12-25 09:36:00
tags:
  - Math
---

## [CF963 E Circles of Waiting](https://codeforces.com/contest/963/problem/E)

主元法：对于一类消元问题，可以先将变量用一组主元表示，然后将这些变量的方程与主元联立求解，以降低复杂度。

对于这道题，我们可以把圆上左边界的点作为主元，具体来说是圆内每行 x 坐标最小的点。那么根据期望方程
$$
F_{x,y}=p_1F_{x-1,y}+p_2F_{x,y-1}+p_3F_{x+1,y}+p_4F_{x,y+1}+1
$$
我们可以得到 $F_{x+1,y}$ 的方程式。这样就可以从左边推到右边界。由于右边界之外的点期望是 0，就可以列出 $2R+1$ 个方程。于是高斯消元即可。时间复杂度 $O(R^3)$。

## [CF235 D Graph Game](https://codeforces.com/contest/235/problem/D)

考虑一棵树的情况。如果在某个时刻我们删掉 $A$ 的时候 $B$ 仍与 $A$ 连通，那么 $(A,B)$ 就会产生 1 的贡献。我们考虑这件事发生的概率。容易发现概率是 A,B 路径长度的倒数（确切地说，这里的长度是指点数）。因此把所有有向路径长度的倒数求和就是答案。

如果是基环树，那么从 A 到 B 就可能会出现两条路径（即中间夹一个环）。那么有两条路径，如果我们直接把贡献算成两条路径长度倒数和的话，会多算贡献。那么减掉路径并的长度倒数即可。

证明见官题。

## [CF457 D Bingo](https://codeforces.com/contest/457/problem/D)

权值是 2 的幂，相当于子集枚举，可以理解其组合意义为每行每列被选择包含的概率的和。因此得到

$$
\frac{1}{m^{\underline{n^2}}\binom{m}{k}}\sum_{i=1}^n\sum_{j=1}^n \binom{n}{i}\binom{n}{j}\binom{m}{k}k^{\underline{n^2-(n-i)(n-j)}}(m-(n^2-(n-i)(n-j)))^{\underline{(n-i)(n-j)}}\\
=\frac{1}{m^{\underline{n^2}}}\sum_{i=1}^n\sum_{j=1}^n \binom{n}{i}\binom{n}{j}k^{\underline{n^2-(n-i)(n-j)}}(m-(n^2-(n-i)(n-j)))^{\underline{(n-i)(n-j)}}
$$
