---
title: 斯特林数学习笔记
date: 2019-11-26 17:32:15
tags:
  - Math
  - Notes
---

斯特林数常用于各类计数问题中。

## 第一类斯特林数

记为 $s(n,k)$，表示把 $n$ 个数分成 $k$ 个圆排列的方案数，有两个递推式：
$$
s(n,k)=s(n-1,k-1)+s(n-1,k)(n-1)\\
s(n,k)=\sum_{i=1}^n \binom{n-1}{i-1}(i-1)!s(n-i,k-1)
$$

第一个递推式的组合意义是枚举最后一个元素的选择；第二个递推式的组合意义是枚举第一个所在排列的状态。我们可以用生成函数的方式理解 $s(n,k)$ 的第一个转移：

$$
G_s(n)=G_s(n-1)x+(n-1)G_s(n-1)\\
    =(x+n-1)G_s(n-1)
$$

因此可以得到
$$
G_s(n)=\sum_{i=0}^ns(n,i)x^i=\prod_{i=0}^{n-1}(x-i)=x^{\underline{n}}
$$
于是可以分治，然后 NTT 求出 $G_s(n)$（不是 DP 的分治 NTT）。

## 第二类斯特林数

记为 $S(n,k)$，表示把 $n$ 个数分成 $k$ 个集合的方案数：

$$
S(n,k)=\sum_{i=1}^nS(n-i,k-1)\binom{n-1}{i-1}\\
S(n,k)=S(n-1,k-1)+S(n-1,k)k
$$

相似地，可以从两个角度理解两个递推式。关于 $S(n,k)$ 有一个恒等式：
$$
m^n=\sum_{i=0}^nS(n,i)i!\binom{m}{i}
$$

$n$ 个不同的物品放到 $m$ 个箱子里。正常数法为 $m^n$。我们还可以枚举有多少个非空盒子，那么我们把 $n$ 个不同的球放到 $i$ 个箱子且非空，则方案数为 $S(n,i)$，然后再枚举这 $i$ 个盒子的位置即可。

上述恒等式也可以理解为斯特林数与下降幂的积：
$$
x^n=\sum_{k=0}^nx^{\underline{k}}S(n,k)\\
x^{\underline{k}}=\prod_{i=0}^{k-1}(x-i)
$$

注意，当 $k>x$ 时有 $x^{\underline{k}}=0$。所以不影响。

## 二项式反演

$$
F(n)=\sum_{i=0}^nG(i)\binom{n}{i}\\
G(n)=\sum_{i=0}^nF(i)(-1)^{n-i}\binom{n}{i}
$$

利用这个反演，我们将上面的恒等式反演一下，设 $F(i)=i^n,G(i)=S(n,i)i!$：
$$
F(m)=\sum_{i=0}^m G(i)\binom{m}{i}\\
$$
这样反演一下可以得到 $S(n,m)$ 的算式：
$$
S(n,m)=\frac{1}{m!}\sum_{i=0}^m(-1)^i\binom{m}{i}(m-i)^n
$$

这个式子也可以用容斥理解。结合 $S(n,m)$ 的组合意义，我们先假设集合有序。然后我们枚举有多少个空的集合，然后我们可以在剩下的集合中随遍填数。最后再除一个 $m!$ 即可。

继续转化可以得到
$$
S(n,m)=\sum_{i=0}^m\frac{(-1)^i}{i!}\cdot\frac{(m-i)^n}{(m-i)!}
$$
这样可以 FFT 计算 $S(n,i),i\in[0,n]$。注意这个恒等式在 $n<m$ 的时候也成立（这时 $S(n,m)=0$）。

[代码](https://gitee.com/sshwy/code/raw/master/luogu/5395.cpp)

## HDU4372

除掉高度为 $n$ 的那个房子，剩下的我们可以把看得到的房子之间的部分与这些看得到的房子理解为一个圆排列。相当于这个组内的房子可以任意排列，但最高的那个一定要放在左边 / 右边，因此这是一个圆排列。于是容易想到第一类斯特林数。

具体地，我们把 $n-1$ 个房子分成 $F+B-2$ 个圆排列后，相当于要选择 $F-1$ 个放左边。因此总方案数为 $\dbinom{F+B-2}{F-1}s(n-1,F+B-2)$。

[代码](https://gitee.com/sshwy/code/raw/master/hdu/4372.cpp)

## TJOI2016 求和

### 解法一

直接推。
$$
\begin{aligned}
&\sum_{i=0}^n\sum_{j=0}^iS(i,j)2^jj!\\
=&\sum_{i=0}^n\sum_{j=0}^nS(i,j)2^jj!\\
=&\sum_{i=0}^n\sum_{j=0}^n\frac{1}{j!}\sum_{k=0}^j\binom{j}{k}(-1)^k(j-k)^i2^jj!\\
=&\sum_{j=0}^n2^j\sum_{k=0}^j\binom{j}{k}(-1)^k\sum_{i=0}^n(j-k)^i\\
=&\sum_{j=0}^n2^jj!\sum_{k=0}^j\frac{(-1)^k}{k!}\cdot \frac{(j-k)^{n+1}-1}{(j-k)!(j-k-1)}\\
\end{aligned}
$$
这样可以直接 FFT 了，复杂度 $O(n\log_2n)$。

### 解法二

这个式子长得很奇怪。设
$$
g(i)=\sum_{j=0}^iS(i,j)2^jj!
$$
考虑其组合意义，相当于把 $i$ 个元素分成若干个集合，集合之间有序，并且每个集合有两种状态，问总方案数。考虑递推，枚举最后一个集合的状态，可以得到
$$
g(i)=\sum_{j=1}^i2\binom{i}{j}g(i-j)
$$
稍作变换可以得到
$$
\frac{g(i)}{i!}=\sum_{j=0}^{i-1}\frac{2}{(i-j)!}\frac{g(j)}{j!}
$$
这样就可以分治 FFT 了。时间复杂度 $O(n\log_2^2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/4555.cpp)

## Luogu4827

> 给一棵 $n$ 个结点边权为 1 的树，对于每个 $i$ 求出
> $$
> \sum_{j=1}^n \text{dist}(i,j)^m
> $$
> $n\le 5\times 10^4,m\le 500$。

考虑到 $x^m=\sum_{i=0}^mx^{\underline{i}}S(n,i)$。因此我们考虑维护每个子树 $u$ 到 $u$ 距离 $m$ 次方和用下降幂展开后的 $S(n,i)$ 的系数，记为 $f(u,i)$。则对于根结点 $root$，答案为 $\sum_{i=0}^mf(root,i)S(n,i)$。因此在此基础上换根即可求出答案。

考虑如何计算 $f(u,i)$。考虑到
$$
(x+1)^{\underline{i}}=x^{\underline{i}}+ix^{\underline{i-1}}
$$
则对于若干条路径 $a_1,a_2,\cdots,a_k$，对 $f(u,i)$ 的贡献为
$$
\sum_{i=1}^ka_i^{\underline{i}}
$$
如果我们将他们都加 1，则贡献变成
$$
\sum_{i=1}^k(a_i+1)^{\underline{i}}=\sum_{i=1}^ka_i^{\underline{i}}+i\sum_{i=1}^ka_i^{\underline{i-1}}
$$
而我们如果添加一条长度为 1 的路径，即把 $a=1$ 加入到序列中，则将会对 $f(u,0)$ 和 $f(u,1)$ 都产生 $1$ 的贡献。$f(u,0)$ 可以理解为是路径数。因此我们得到了 $f(u,i)$ 的转移式：
$$
f(u,i)\gets\sum_{v\in Son(u)}f(v,i)+i\cdot f(v,i-1)\\
f(u,1)\gets|Son(u)|\\
f(u,0)\gets|Son(u)|
$$
注意处理上边界的转移，对于子树中最长的链，要把长度的阶乘贡献到对应的 DP 值上。

然后再换根即可。

时间复杂度 $O(nm)$。

[代码](https://gitee.com/sshwy/code/raw/master/luogu/4827.cpp)

## CF961G

可以直接写出一个暴力式子：
$$
\sum_{i=1}^nw_i\sum_{j=1}^n\binom{n-1}{j-1}S(n-j,k-1)j
$$
两边可以分别计算。然后做一下灵魂转化：
$$
\begin{aligned}
&\sum_{j=1}^n\binom{n-1}{j-1}S(n-j,k-1)j\\
=&\sum_{j=1}^n\binom{n-1}{j-1}S(n-j,k-1)+\sum_{j=1}^n\binom{n-1}{j-1}S(n-j,k-1)(j-1)\\
=&\sum_{j=1}^n\binom{n-1}{j-1}S(n-j,k-1)+(n-1)\sum_{j=1}^n\binom{n-2}{j-2}S(n-j,k-1)\\
=&S(n,k)+(n-1)S(n-1,k)
\end{aligned}
$$
这样算两个斯特林数即可。复杂度 $O(n\log_2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/codeforces/961g.cpp)

## BZOJ5093 图的价值

对每个点的贡献分别计算：
$$
\begin{aligned}
Ans=&n\sum_{i=0}^{n-1}\binom{n-1}{i}2^{\binom{n-1}{2}}i^k\\
=&n2^{\binom{n-1}{2}}\sum_{i=0}^{n-1}\binom{n-1}{i}i^k
\end{aligned}
$$

然后我们变换一下后面的式子（假设 n 减了 1）：
$$
\begin{aligned}
&\sum_{i=0}^n\binom{n}{i}i^k\\
=&\sum_{i=0}^n\binom{n}{i}\sum_{j=0}^k\binom{i}{j}j!S(k,j)\\
=&\sum_{i=0}^n\sum_{j=0}^k\frac{n!}{(n-i)!(i-j)!}S(k,j)\\
=&\sum_{i=0}^n\sum_{j=0}^k\binom{n-j}{i-j} n^{\underline{j}}S(k,j)\\
=&\sum_{j=0}^kn^{\underline{j}}S(k,j)\sum_{i=j}^n\binom{n-j}{i-j} \\
=&\sum_{j=0}^kn^{\underline{j}}S(k,j)2^{n-j}
\end{aligned}
$$
我们 $O(n\log_2n)$ 计算出 $S(k,j),j\in[0,k]$ 即可。复杂度 $O(n\log_2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/5093.cpp)

## BZOJ4671 异或图

考虑容斥。

设 $f(i)$ 表示在所有连通块个数大于等于 $i$ 的方案（即异或出来的图）中，将连通块划分为 $i$ 个集合的方案数。定义比较奇怪，但考虑如何计算。

对于点集，考虑枚举所有的集合划分方案（方案数与贝尔数同阶），同一个集合内的点任意连通，而不同集合的点强制**不连通**。问满足这个条件的异或图的个数。

考虑线性基，我们只考虑跨过集合的边对应的位，把图插入到线性基中，问题转化为求异或和为 0 的方案数，显然为 $2^{S-tot}$，其中 $tot$ 表示线性基的大小。因为线性基外的图任意异或，都可以用线性基里的图唯一地异或出一个反图，使得异或和为 0。

于是我们枚举所有的集合划分方案，$O(Sn^2)$ 计算贡献，贡献到对应的 $f$ 上。这样就计算出了 $f$。我们现在要求的是 $g(i)$，表示连通块个数恰好为 $i$ 的方案数（等价于在所有连通块个数**等于**$i$ 的方案中，将连通块划分为 $i$ 个集合的方案数）。考虑到
$$
f(i)=\sum_{x=i}^nS(x,i)g(x)
$$
根据斯特林反演得到
$$
g(i)=\sum_{x=i}^ns(x,i)(-1)^{x-i}f(x)
$$
则得到
$$
g(1)=\sum_{x=1}^n(x-1)!(-1)^{x-1}f(x)
$$
于是我们在实现的时候不需要斯特林反演，直接做即可。

时间复杂度 $O(B_nSn^2)$。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/4671.cpp)

