---
title: 生成函数与多项式基础
date: 2019-12-11 08:13:00
tags:
  - Math
  - Polynomial-Series
  - Notes
---

## 普通生成函数

序列 $a_i$ 的普通生成函数（Ordinary generating function，OGF）：

$$
F(x)=\sum_{i\ge 0}a_ix^i
$$


举个例子，等比数列的生成函数为

$$
\sum_{i\ge 0}x^i=\frac{1}{1-x}\\
\sum_{i\ge 0}a^ix^i=\frac{1}{1-ax}\\
$$

这也是 OGF 的常用封闭形式。

## 牛顿二项式定理

首先我们定义组合数的计算：

$$
\binom{r}{k}=\frac{r^{\underline{k}}}{k!}(r\in \mathbb{C},k\in \mathbb{N})
$$

这样就可以扩展二项式定义为

$$
(1+x)^\alpha=\sum_{k\ge 0}\binom{\alpha}{k}x^{k}
$$

## 卡特兰数的生成函数

考虑卡特兰数的递推式
$$
h(i)=\sum_{j=0}^{i-1}h(j)h(i-j-1)
$$

### 第一部分

其生成函数定义为
$$
\begin{aligned}
C(x)&=\sum_{i\ge 0}h(i)x^i\\
&=1+\sum_{i\ge 1}\sum_{j=0}^{i-1}h(j)x^jh(i-j-1)x^{i-j-1}x\\
&=1+x\sum_{j\ge 0}h(j)x^j\sum_{i\ge 0}h(i)x^{i}\\
&=1+xC^2(x)
\end{aligned}
$$
那么解方程得到
$$
C(x)=\frac{1\pm \sqrt{1-4x}}{2x}
$$
正根不收敛，因此舍掉，那么我们就得到了
$$
C(x)=\frac{1-\sqrt{1-4x}}{2x}
$$

### 第二部分

接下来我们将它展开为无穷级数。
$$
(1-4x)^{\frac{1}{2}}=\sum_{i\ge 0}\binom{\frac{1}{2}}{i}(-4x)^i\\
=1+\sum_{i\ge 1}\frac{\left(\frac{1}{2}\right)^{\underline{i}}}{i!}(-4x)^i \tag{1}
$$

化简下降幂的分式：

$$
\frac{\left(\frac{1}{2}\right)^{\underline{i}}}{i!}=(-1)^{i-1}\frac{1}{2^i}\frac{(2i-3)!!}{i!} \tag{2}
$$

化简双阶乘：

$$
(2i-3)!!=\frac{(2i-2)!}{(2i-2)(2i-4)\cdots 2}=\frac{(2i-2)!}{2^{i-1} (i-1)!}
$$

于是 $(2)$ 就变为

$$
\frac{\left(\frac{1}{2}\right)^{\underline{i}}}{i!}=(-1)^{i-1}\frac{1}{2^{2i-1}}\frac{(2i-2)!}{(i-1)!i!} \tag{3}
$$

把 $(3)$ 带回 $(1)$ 得到

$$
(1-4x)^{\frac{1}{2}}=1+\sum_{i\ge 1}\frac{\left(\frac{1}{2}\right)^{\underline{i}}}{i!}(-4x)^i \\
=1-\sum_{i\ge 1}2\frac{(2i-2)!}{(i-1)!i!}x^i \tag{4}\\
$$

把 $(4)$ 带回原式得到

$$
\begin{aligned}
C(x)&=\sum_{i\ge 1}\frac{(2i-2)!}{(i-1)!i!}x^{i-1}\\
&=\sum_{i\ge 0}\frac{(2i)!}{i!(i+1)!}x^{i}\\
&=\sum_{i\ge 0}\frac{1}{i+1}\binom{2i}{i}x^{i}
\end{aligned}
$$

## BZOJ3028 食物

生成函数依次写成

$$
\frac{1}{1-x^2}(1+x)\frac{1-x^3}{1-x}\frac{x}{1-x^2}\frac{1}{1-x^4}\frac{1-x^4}{1-x}(1+x)\frac{1}{1-x^3}
$$

化简得到

$$
\frac{x}{(1-x)^4}
$$

根据牛顿二项式定理得到

$$
\begin{aligned}
(1-x)^{-4}&=\sum_{i\ge 0}\binom{-4}{i}(-x)^i\\
&=\sum_{i\ge 0}\binom{4+i-1}{i}x^i
\end{aligned}
$$

那么带回原式得到

$$
\begin{aligned}
\frac{x}{(1-x)^4}&=\sum_{i\ge 0}\binom{3+i}{i}x^{i+1}\\
&=\sum_{i\ge 0}\binom{3+i}{3}x^{i+1}\\
&=\sum_{i\ge 1}\binom{2+i}{3}x^i
\end{aligned}
$$

## BZOJ3027 Sweet

生成函数为

$$
\prod_{i=1}^n\left(\sum_{j=0}^{m_i}x^j\right)\\
=\prod_{i=1}^n\frac{1-x^{m_i+1}}{1-x}\\
=(1-x)^{-n}\prod_{i=1}^n(1-x^{m_i+1})\\
$$

根据牛顿二项式定理得到

$$
(1-x)^{-n}=\sum_{i\ge 0}\binom{n+i-1}{i}x^i
$$

我们暴力展开后面的多项式，最多只有 $2^n$ 项。考虑每一项对答案的贡献，发现 $c_ix^i$ 的贡献是

$$
c_i\sum_{j=a-i}^{b-i}\binom{n+j-1}{n-1}
$$

这样就做完了。

## 常见导数

$$
\begin{aligned}
(x^n)'&=nx^{n-1}\\
|x|'&=\operatorname{sgn}x\\
(e^x)'&=e^x\\
(a^x)'&=e^{x\ln a}\ln a\\
(\ln x)'&=\frac{1}{x}
\end{aligned}
$$

## 多项式积分

$$
\int F(x)\text{d}x=\int\sum_{i\ge 0}a_ix^i\text{d}x=\sum_{i\ge 1}\frac{a_{i-1}}{i}x^i
$$

## 泰勒展开

泰勒展开可以将一个函数表示为无穷级数的形式：
$$
f(x)=\sum_{n\ge 0}\frac{f^{(n)}(x_0)}{n!}(x-x_0)^n
$$

## 多项式 ln

对于一个多项式 $F(x)$，$\ln F(x)$ 的定义是什么？

首先我们考虑 $\ln (1-x)$ 在原点的泰勒展开：
$$
\ln(1-x)=-\sum_{i\ge 1}\frac{x^i}{i}
$$
这样一来，我们就可以定义 $\ln (1-F(x))$ 了：

$$
\ln(1-F(x))=-\sum_{i\ge 1}\frac{F(x)^i}{i}
$$

那么对于一个多项式 $F(x)$ 如何求出 $\ln F(x)\pmod{x^n}$？考虑复合函数求导：

$$
(\ln F(x))'=\frac{\text{d}\ln F(x)}{\text{d}x}=\frac{\text{d}\ln F(x)}{\text{d}F(x)}\frac{\text{d}F(x)}{\text{d}x}=\frac{F'(x)}{F(x)}
$$

两边同时积分得到

$$
\ln F(x)=\int\frac{F'(x)}{F(x)}\text{d}x
$$

这样就可以用多项式求逆来计算了。这样计算的一个问题是，求导再积分后，常数项就没了。这时如果 $F(0)=1$ 就会比较好办，这时 $\ln F(0)=\ln 1=0$。

注：
$$
\ln x=\sum_{i\ge 1}\frac{1}{i}\left( \frac{x-1}{x} \right)^i\qquad \forall Re(x)\ge \frac{1}{2}.
$$
所以不要尝试用 $\ln x$ 来理解 $\ln F(x)$……

## 多项式牛顿迭代

已知函数 $G$，求多项式 $F$，使得

$$
G(F(x))=0\pmod{x^n}
$$

考虑倍增。假设我们求出了 $F_0$ 使得 $G(F_0(x))=0\pmod{x^{\left\lceil\frac{n}{2}\right\rceil}}$。

首先注意到 $G(F(x))=0\pmod{x^{\left\lceil\frac{n}{2}\right\rceil}}$，因此 $F\bmod x^{\left\lceil\frac{n}{2}\right\rceil}=F_0$，即他们的低位是一样的。

考虑将 $G(F(x))$ 在 $F_0(x)$ 处泰勒展开：

$$
G(F(x))=\sum_{i\ge 0}\frac{G^{(i)}(F_0(x))}{i!}(F(x)-F_0(x))^i\pmod{x^n}
$$

注意到 $F(x)-F_0(x)$ 把低位都减没了。$(F(x)-F_0(x))^2$ 的最低次数项的次数都是 $x^n$，因此可以直接忽略了。即

$$
G(F(x))=G(F_0(x)) + G^{'}(F_0(x))(F(x)-F_0(x))\pmod{x^n}
$$

因为 $G(F(x))=0$，化简得到

$$
F(x)=F_0(x)-\frac{G(F_0(x))}{G^{'}(F_0(x))}  \pmod{x^n}
$$

也可以简记为

$$
F=F_0-\frac{G\circ F_0}{G'\circ F_0}\pmod{x^n}
$$

接下来我们来看一下多项式牛顿迭代的应用。

### 多项式求逆

假设我们要求 $h(x)$ 的逆。设 $G(F(x))=F^{-1}(x)-h(x)$。

那么问题转化为了求满足 $G(F(x))=0\pmod{x^n}$ 的 $F(x)$ 的值。应用牛顿迭代得到

$$
\begin{aligned}
F(x)&=F_0(x)-\frac{G(F_0(x))}{G'(F_0(x))}\\
&=F_0(x)-\frac{F_0^{-1}(x)-h(x)}{-F_0^{-2}(x)}\\
&=2F_0(x)-h(x)F_0^2(x)
\end{aligned}
$$

那么使用 FFT/NTT 即可在 $O(n\log_2n)$ 的时间内计算。

### 多项式开方

假设我们要求 $h(x)$ 的平方根。设 $G(F(x))=F^2(x)-h(x)$。应用牛顿迭代得到

$$
\begin{aligned}
F(x)&=F_0(x)-\frac{G(F_0(x))}{G'(F_0(x))}\\
&=F_0(x)-\frac{F_0^2(x)-h(x)}{2F_0(x)}\\
&=\frac{F_0^2(x)+h(x)}{2F_0(x)}
\end{aligned}
$$

时间复杂度 $O(n\log_2n)$。

### 多项式 exp

对于一个多项式 $F(x)$，$\exp F(x)$ 的含义是什么？

仿照 $\ln F(x)$ 的含义，我们将 $\exp x=e^x$ 在原点泰勒展开得到

$$
\exp x=\sum_{i\ge 0}\frac{e^0}{i!}x^i=\sum_{i\ge 0}\frac{x^i}{i!}
$$

因此可以得到

$$
\exp F(x)=\sum_{i\ge 0}\frac{F^i(x)}{i!}
$$

考虑如何求 $\exp H(x)\pmod {x^n}$。设 $G(F(x))=\ln F(x)-H(x)\pmod{x^n}$。应用牛顿迭代得到

$$
\begin{aligned}
F(x)&=F_0(x)-\frac{G(F_0(x))}{G'(f_0(x))}\\
&=F_0(x)-\frac{\ln F_0(x)-H(x)}{\frac{1}{F_0(x)}}\\
&=F_0(x)(1-\ln F_0(x)+H(x))
\end{aligned}
$$

时间复杂度 $O(n\log_2n)$。

[模板代码](https://gitee.com/sshwy/code/raw/master/mb/polynomial.cpp)

## 参考文献

[泰勒级数 - 维基百科](https://zh.wikipedia.org/wiki/%E6%B3%B0%E5%8B%92%E7%BA%A7%E6%95%B0)

[多项式牛顿迭代 - OI Wiki](https://oi-wiki.org/math/poly/newton/)

[浅谈生成函数之 OGF](https://zhuanlan.zhihu.com/p/52817010)

[牛顿迭代法在多项式运算的应用 - Miskcoo's Space](http://blog.miskcoo.com/2015/06/polynomial-with-newton-method#i-4)

[导数列表 - 维基百科](https://zh.wikipedia.org/wiki/%E5%AF%BC%E6%95%B0%E5%88%97%E8%A1%A8)

[指数函数 - 维基百科](https://zh.wikipedia.org/wiki/%E6%8C%87%E6%95%B0%E5%87%BD%E6%95%B0])
