---
title: 集合变换学习笔记
date: 2020-03-28 15:10:14
tags:
  - Math
  - Notes
  - FWT
  - FMT
---

总结一下有关子集变换的笔记。

## 快速莫比乌斯变换

### 子集与

考虑形式化的问题：对于两个序列 $a,b$，我们希望求
$$
c_i=\sum_{j\wedge k=i}a_ib_k
$$
更一般的，我们想求的是
$$
c(S)=\sum_{S=P\cap Q}a(P)b(Q)
$$
其中 $S$ 是集合。接下来的问题与过程形式都使用集合来展现。

考虑做一个高维后缀和的变换：
$$
f'(S)=\sum_{S\subseteq T}f(T)
$$
则可以得到
$$
\begin{aligned}
c'(S)&=\sum_{S\subseteq T}c(T)=\sum_{S\subseteq P\cap Q}a(P)b(Q)\\
&=\sum_{S\subseteq P}a(P)\sum_{S\subseteq Q}b(Q)\\
&=a'(S)b'(S)
\end{aligned}
$$
高维后缀和可以写成

```cpp
for(int i=0;i<n;i++){
    for(int j=0;j<(1<<n);j++)
        if((j&(1<<i))==0)
            s[j]+=s[j+(1<<i)];
}
```

在求出了 $c'$ 后，我们希望还原成 $c$，可以用容斥得到
$$
c[T]=\sum_{T\subseteq S}(-1)^{|T|-|S|}c'[S]
$$

但这样做未免麻烦。直接做高维后缀差分即可。

```cpp
for(int i=0;i<n;i++){
    for(int j=0;j<(1<<n);j++)
        if((j&(1<<i))==0)
            s[j]-=s[j+(1<<i)];
}
```

### 子集或

我们考虑一个一般化的问题。对于序列 $a,b$，我们想求
$$
c(S)=\sum_{S=P\cup Q}a(P)b(Q)
$$
同样的，我们这次做高维前缀和
$$
f'(S)=\sum_{T\subseteq S}f(S)
$$
则可以推出
$$
\begin{aligned}
c'(S)&=\sum_{T\subseteq S}c(T)=\sum_{P\cup Q\subseteq S}a(P)b(Q)\\
&=\sum_{P\subseteq S}a(P)\sum_{Q\subseteq S}b(Q)\\
&=a'(S)b'(S)
\end{aligned}
$$
于是使用高维前缀和与高维前缀差分即可。

### 扩域情况

如果每一维是一个向量？
$$
c(\min(a_1,b_1),\min(a_2,b_2),\cdots,\min(a_n,b_n))
=\sum a(a_1,a_2,\cdots,a_n)b(b_1,b_2,\cdots,b_n)\\
c'(p_1,p_2,\cdots,p_n)=\sum_{p_i\le q_i}c(q_1,q_2,\cdots,q_n)
$$

取 $\min$ 相当于与运算；$p_i\le q_i$ 相当于后缀和操作。

## 分治多项式乘法

我们有两个多项式 $A,B$，我们想求 $A(x)B(x)$。设
$$
A(x)=P_1(x)x^{n/2}+P_2(x)\\
B(x)=Q_1(x)x^{n/2}+Q_2(x)
$$
假定 $2|n$。则我们有


$$
A(x)B(x)=(P_1(x)x^{n/2}+P_2(x))(Q_1(x)x^{n/2}+Q_2(x))\\
=(P_1Q_1)x^n+(P_1Q_2+P_2Q_1)x^{n/2}+P_2Q_2\\
$$

而我们知道
$$
P_1Q_2+P_2Q_1=(P_1+Q_1)(P_2+Q_2)-P_1Q_1-P_2Q_2\\
$$
因此可以只算 $P_1Q_1,P_2Q_2,(P_1+Q_1)(P_2+Q_2)$ 的乘法，递归下去。时间复杂度
$$
T(n)=3T(n/2)+O(n)=O(n^{\log_23})\\
$$

## 快速沃尔什变换

### 异或卷积

考虑形式化的问题。对于 $a,b$，我们要求
$$
c_i=\sum_{j\oplus k=i}a_jb_k
$$
由于异或具有结合律、交换律，对加法的分配律，则我们用多项式乘法表示上述过程。即
$$
C(x)=\sum c_ix^i
$$
（可以理解为在合并系数和指数的时候，加法是异或运算）设
$$
A(x)=P_1x^{n/2}+P_2\\
B(x)=Q_1x^{n/2}+Q_2
$$
则使用分治多项式乘法可以得到
$$
\begin{aligned}
C(x)&=A(x)\oplus B(x)\\
&=(P_1x^{n/2}+P_2)\oplus(Q_1x^{n/2}+Q_2)\\
&=(P_1Q_1+P_2Q_2)+(P_2Q_1+P_1Q_2)x^{n/2}
\end{aligned}
$$
（可以理解为，$P_1x^{n/2}Q_1x^{n/2}=P_1Q_1x^{(n/2)\oplus (n/2)}=P_1Q_1$）我们设
$$
X=(P_1+P_2)\oplus(Q_1+Q_2)\\
Y=(P_1-P_2)\oplus(Q_1-Q_2)
$$
于是可以得到
$$
P_1Q_1+P_2Q_2=\frac{X+Y}{2}\\
P_2Q_1+P_1Q_2=\frac{X-Y}{2}
$$
这样时间复杂度就是
$$
T(n)=2T(n/2)+O(n)=O(n\log_2n)\\
$$

这就是异或卷积本质上在做的事情。事实上，与卷积和或卷积都可以使用分治多项式乘法去理解。

### 异或卷积与 FWT

FWT 和 IFWT 的递归过程为
$$
\begin{array}{r|l|r|l}
\hline
1 & \textbf{function }\text{FWT}(a,l,r)&
1 & \textbf{function }\text{IFWT}(a,l,r)\\
2 & \qquad m \gets \frac{r-l}{2}&
2 & \qquad \text{IFWT}(a,l,l+m)\\
3 & \qquad \textbf{for }i=l\textbf{ to }l+m-1&
3 & \qquad \text{IFWT}(a,l+m,r)\\
4 & \qquad \qquad x \gets a_i&
4 & \qquad m \gets \frac{r-l}{2}\\
5 & \qquad \qquad y \gets a_{i+m}&
5 & \qquad \textbf{for }i=l\textbf{ to }l+m-1\\
6 & \qquad \qquad a_i \gets x+y&
6 & \qquad \qquad x \gets a_i\\
7 & \qquad \qquad a_{i+m} \gets x-y&
7 & \qquad \qquad y \gets a_{i+m}\\
8 & \qquad \textbf{end for} &
8 & \qquad \qquad a_i \gets \frac{x+y}{2}\\
9 & \qquad \text{FWT}(a,l,l+m)&
9 & \qquad \qquad a_{i+m} \gets \frac{x-y}{2}\\
10 & \qquad \text{FWT}(a,l+m,r)&
10 & \qquad \textbf{end for} \\
11 & \textbf{end function} &
11 & \textbf{end function} \\
  \hline
\end{array}
$$
异或卷积则是 FWT 后直接做乘法：$\operatorname{IFWT}( \operatorname{FWT}(a) \times \operatorname{FWT}(b) )$。

事实上， $\operatorname{IFWT}(a) = \operatorname{FWT}(a)\cdot 2^{-k}$。因此非递归版本的 IFWT 也可以直接在最后除掉一个 $2^k$。

### 异或 FWT 本质

FWT 的本质是把 $a$ 变成了
$$
\text{FWT}(a)_i=\sum_j a_j (-1)^{\operatorname{popcount}(i\operatorname{and} j)}
$$

写成集合幂级数的形式就是
$$
\text{FWT}(a)(S)=\sum_{T}a(T) (-1)^{|S\cap T|}
$$

有一个小 Trick 就是，如果我们修改 $\operatorname{FWT}(a)(0)$，那么等 $\operatorname{IFWT}$ 变换回去后，所有项都被增加一个常量（这个常量不一定等于我们修改的差量）。原因如上所述，$a_0$ 的贡献永远是正的。

这个性质可以用来解一些要求 $a_0=0$ 的题，那么我们给 $\operatorname{FWT}(a)(0)$ 随机一个值，变换回去后把所有项都减掉 $a_0$ 即可。

[模板代码](https://gitee.com/sshwy/code/raw/master/mb/fwt.cpp)

## 子集卷积

我们考虑这样一个问题：求不相交的或卷积：

$$
c(S)=\sum_{P\cup Q=S,P\cap Q=\varnothing}a(P)b(Q)
$$

这个不好求。则我们设
$$
A(x,S)=\sum_{T\subseteq S,|T|=x}a(T)\\
B(x,S)=\sum_{T\subseteq S,|T|=x}b(T)
$$
则可以得到
$$
\begin{aligned}
C(x,S)&=\sum_{y+z=x}A(y,S)B(z,S)\\
&=\sum_{y+z=x}\sum_{P\subseteq S,|P|=y}\sum_{Q\subseteq S,|Q|=z}a(P)b(Q)\\
&=\sum_{P\subseteq S,Q\subseteq S,|P|+|Q|=x}a(P)b(Q)\\
&=\sum_{P\cup Q\subseteq S,|P|+|Q|=x}a(P)b(Q)\\
\end{aligned}
$$
我们设
$$
c(x,S)=\sum_{P\cup Q=S,|P|+|Q|=x}a(P)b(Q)
$$
则答案显然为 $c(|S|,S)$。因为 $|P|+|Q|=|P\cup Q|+|P\cap Q|\ge |P\cup Q|=|S|$，所以当且仅当 $P\cap Q=\varnothing$ 时有 $|P|+|Q|=|S|$。于是
$$
\sum_{T\subseteq S}c(x,T)=C(x,S)\\
c(x,S)=C(x,S)-\sum_{T\subset S}c(x,T)
$$
于是做一个前缀差分即可。

总结来说，我们将 $a(S),b(S)$ 变成 $A(x,S),B(x,S)$，然后暴力卷积计算 $C(x,S)$，最后还原成对应的 $c(x,T)$。设全集为 $[n]$，则复杂度为
$$
2O(n2^n)+O(n^22^n)+O(n2^n)=O(n^22^n)
$$

在实现的时候有一个技巧，就是在暴力卷积的时候要稍微调整循环顺序，不要一列一列访问内存，不然常数极大。

[代码](https://gitee.com/sshwy/code/raw/master/loj/152.cpp)

### HDU 5823

首先显然色数是 $\le n$ 的，因为你每个点染不同颜色一定是成立的。

而显然每种颜色可以染一个独立集。设 $f(S)$ 表示点集 $S$ 是否是一个独立集，$g(S,x)$ 表示 $S$ 点集能否使用 $x$ 种颜色染。则

$$
g(S,1)=f(S)\\
g(S,i)=\left[\sum_{T\subseteq S}g(T,i-1)f(S\setminus T)\right]\\
g(P\cup Q,i)=\left[\sum_{P\cap Q=\varnothing}g(P,i-1)f(Q)\right]
$$

但事实上 $P\cap Q=\varnothing$ 不是必要条件，它不会影响最优解。因此这就是一个子集或卷积，需要做 $n$ 次，每次卷完要回来转成艾弗森括号运算值。总复杂度 $O(n^22^n)$。

[代码](https://gitee.com/sshwy/code/raw/master/hdu/5823.cpp)

### CF1034 E

> 求 $c(S)=\sum_{P\cup Q=S,P\cap Q=\varnothing}a(P)b(Q)\bmod 4$。

可以使用上述子集卷积做法，复杂度 $O(n^22^n)$，在这道题中是不能通过的。

设

$$
A(S)=\sum_{T\subseteq S}a(T)p^{|T|}\\
B(S)=\sum_{T\subseteq S}b(T)p^{|T|}
$$

且令

$$
F(S)=A(S)B(S)
=\sum_{P\cup Q\subseteq S}a(P)b(Q)p^{|P|+|Q|}
$$

设

$$
f(S)=\sum_{P\cup Q=S} a(P)b(Q)p^{|P|+|Q|}
$$

容易发现

$$
\sum_{T\subseteq S}f(T)=F(S)
$$

所以可以子集前缀差分算出 $f(S)$。

而注意到当 $P\cup Q=S,P\cap Q\ne \varnothing$ 时，$|P|+|Q|>|S|$。因此 $p^{|S|}\mid p^{|P|+|Q|}$，于是

$$
\begin{aligned}
c(S)\equiv &\frac{f(S)}{p^{|S|}}\\
&=\frac{\sum_{P\cup Q=S}a(P)b(Q)p^{|P|+|Q|}}{p^{|S|}}\\
&=\frac{\sum_{P\cup Q\subseteq S,P\cap Q=\varnothing}a(P)b(Q)p^{|S|}}{p^{|S|}}\\
&=\sum_{P\cup Q\subseteq S,P\cap Q=\varnothing}a(P)b(Q) \pmod p
\end{aligned}
$$

算的时候开 LL 暴力右移即可。

[代码](https://gitee.com/sshwy/code/raw/master/codeforces/1034/e/e.cpp)

### HDU 5330

这道题虽然名称上是状圧 DP，但在 DP 的过程中是按维度考虑，其实是在做一个高维前缀和的过程。这样就更容易理解它 DP 的含义了。

具体的 DP 方法可以见注释。

[代码](https://gitee.com/sshwy/code/raw/master/hdu/5330.cpp)

### AGC034 F

设 $E(x)$ 表示从 $0$ 变到 $x$ 的期望：
$$
E(x)=1+\sum_{i=0}^{2^n-1}E(i)p_{x\oplus i}\\
=1+\sum_{i=0}^{2^n-1}E(x\oplus i)p_i \\
$$
且 $p_x=\dfrac{A_x}{S},E(0)=0$。注意到这是一个异或卷积的形式（$x\oplus i\oplus i=x$），则这个等式可以被表示为
$$
\left\langle E(0),E(1),\cdots,E(2^n-1) \right\rangle \oplus \left\langle p_0,p_1,\cdots,p_{2^n-1} \right\rangle=\left\langle ?,E(1)-1,E(2)-1,\cdots,E(2^n-1)-1 \right\rangle
$$
$E(0)=0$，不满足和式，因此表示为 $?$ 状态。

由于 $\sum_{i=0}^{2^n-1}p_i=1$，而在上述异或卷积中每个 $E(i)$ 都和所有的 $p_i,i\in[0,2^n-1]$ 乘起来贡献了一次。因此
$$
E(0)+E(1)+\cdots+E(2^n-1)=?+(E(1)-1)+(E(2)-1)+\cdots +(E(2^n-1)-1)\\
?=E(0)+2^n-1
$$

于是得到
$$
\left\langle E(0),E(1),\cdots,E(2^n-1) \right\rangle \oplus \left\langle p_0,p_1,\cdots,p_{2^n-1} \right\rangle\\
=\left\langle E(0)+2^n-1,E(1)-1,E(2)-1,\cdots,E(2^n-1)-1 \right\rangle
$$
我们想办法把所有 $E(x)$ 消掉：
$$
\left\langle E(0),E(1),\cdots,E(2^n-1) \right\rangle \oplus \left\langle p_0-1,p_1,\cdots,p_{2^n-1} \right\rangle=A=\left\langle 2^n-1,-1,-1,\cdots,-1 \right\rangle
$$
于是我们 FWT 之后计算 $\operatorname{FWT}(A)(i)/\operatorname{FWT}(p)(i)$，然后再转回来即可。因为最后的答案是强制 $E(0)=0$，所以在做的时候可以直接乘上逆元。最后使用 FWT 本质的 Trick 即可。

时间复杂度 $O(2^nn)$。

[代码](https://gitee.com/sshwy/code/raw/master/atcoder/agc034_f.cpp)

