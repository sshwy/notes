---
title: 期望进阶
date: 2019-12-09 09:12:00
tags:
  - Math
---

## [Topcoder12984 TorusSailing](https://vjudge.net/problem/TopCoder-12984)

考虑矩阵的最后一行和最右一列，我们把所有方格的期望表示为这 $n+m-1$ 个变量的方程。

然后考虑矩阵的第一行和最左一列，发现可以联立这些方程与最后一行、最右一列的方程。

这样就可以高斯消元了。消完了再带回去即可求出期望。

时间复杂度 $O((n+m)^3)$。

[代码](https://gitee.com/sshwy/code/raw/master/topcoder/12984.cpp)

## [HDU4035 Maze](http://acm.hdu.edu.cn/showproblem.php?pid=4035)

设 $f(u)$ 表示结点 $u$ 的期望，那么

$$
f(u)=k_uf(1)+\frac{1-k_u-e_u}{d_u}\sum_{(u,v)}(f(v)+1)\\
$$

我们尝试将 $f(u)$ 表示成 $a_uf(p)+b_u+c_uf(1)$ 的形式，其中 $p$ 是 $u$ 的父节点。首先设

$$
A_u=\sum_{v\in Son(u)}a_v\\
B_u=\sum_{v\in Son(u)}b_v\\
C_u=\sum_{v\in Son(u)}c_v\\
D_u=\frac{d_u}{1-k_u-e_u}\\
$$

那么把上式化简可以得到

$$
f(u)=\frac{f(p)}{D_u-A_u}+\frac{B_u+d_u}{D_u-A_u}+\frac{D_uk_u+C_u}{D_u-A_u}f(1)\\
$$

对于根结点的情况：

$$
f(1)=\frac{B_1+d_1}{D_1-k_1D_1-A_1-C_1}
$$

实现的时候注意判无解即可。注意，本题精度要开到 $10^{-9}$。

[代码](https://gitee.com/sshwy/code/raw/master/hdu/4035.cpp)

## SHOI2017 分手是祝愿

考虑按灯泡的最优策略。我们显然是先找到最大的 1 把它变成 0，然后再找最大的 1 变成 0……

因此设 $f(i)$ 表示把一个最优按 $i$ 次能全 0 的局面变成按 $i-1$ 次能全 0 的局面的期望步数。显然，当 $i\le k$ 时你会选最优，即 $f(i)=1$；否则，你有 $\dfrac{i}{n}$ 的概率按到一个应该按的灯，剩余的概率则会转移到 $f(i+1)$。因此得到

$$
f(i)=\frac{i}{n} + \frac{n-i}{n}(f(i)+f(i+1)+1)
$$

边界情况 $f(n)=1,f(0)=0$。$f(n)=1$ 是因为你不论按哪个，都能按到你应该按的那个灯。

于是我们先求出初始局面在最优情况下需要按的次数，假设为 $x$。那么答案为

$$
n!\sum_{i=1}^x f(i)
$$

时间复杂度 $O(n\log_2n)$ 或 $O(n\sqrt{n})$。

[代码](https://gitee.com/sshwy/code/raw/master/loj/2145.cpp)

## CTSC2006 歌唱王国

### DP 思路

设 $f(i)$ 表示长度为 $i$ 时恰好结束的概率，$g(i)$ 表示长度为 $i$ 时没有结束的概率。首先
$$
f(i)+g(i)=g(i-1)\\
f(0)+g(0)=1
$$
即在 $i-1$ 的长度还没有结束，那么可能会在 $i$ 或者大于 $i$ 的长度结束。当 $i=0$ 时是特殊情况。

对于这个串 $S$，我们记录 $a_i$ 表示 $S[1,i]$ 与 $S[|S|-i+1,|S|]$ 是否匹配（即是否是 border）。那么我在长度为 $i$ 的状态下直接加入这个串，显然一定会结束。但是，我们还可能会在长度小于 $i+|S|$ 的时候结束（即当我补完一个 border 的时候就结束了）。那么可以得到
$$
g(i)\frac{1}{n^{|S|}}=\sum_{j=1}^{|S|}a_jf(i+j)\frac{1}{n^{|S|-j}}
$$
解释一下上式的含义。

左边：我强行插入 $S$ 这个字符串，在长度大于 $i$，小于等于 $i+|S|$ 时结束的概率。

右边：如果 $S[1,j]$ 是一个 border，那么我们在补后面的 $|S|-j$ 个字符的过程中，在长度小于等于 $i+|S|$ 的时候结束的概率。

也可以理解为，左边的式子里，每一种结束的概率都会被它在 $[i,i+|S|]$ 内的 border 多算几次。

接下来我们用概率生成函数来改写上式。

### 概率生成函数

离散随机变量 $X$ 的概率生成函数为
$$
F_X(x)=\sum_{i=0}^\infty P(X=i)x^i
$$
该函数有两个重要的性质：

1. $F_X(1)=\sum_{i=0}^\infty P(X=i)=1$；
2. $F_X'(1)=\sum_{i=0}^\infty P(X=i)\cdot i=E(X)$。

那么设 $F(x)=\sum_{i=0}^\infty f(i)x^i$，$G(x)=\sum_{i=0}^\infty g(i)x^i$（注意，F 是概率生成函数，但 G 不是）。

### 第一部分

于是我们可以把
$$
f(i)+g(i)=g(i-1)\\
f(0)+g(0)=1
$$
改写为
$$
F(x)+G(x)=G(x)\cdot x+1
$$
我们对两边求导得到
$$
F'(x)+G'(x)=G'(x)+G(x)\\
F'(x)=G(x)
$$
我们要求的是 $E(X)=F'(1)$，因此 $F'(1)=G(1)$。

### 第二部分

把
$$
g(i)\frac{1}{n^{|S|}}=\sum_{j=1}^{|S|}a_jf(i+j)\frac{1}{n^{|S|-j}}
$$
改写为
$$
G(x)\left( \frac{x}{n} \right)^{|S|}=\sum_{j=1}^{|S|}a_jF(x)\left(\frac{x}{n}\right)^{|S|-j}
$$
那么令 $x=1$ 可以得到
$$
G(1) \frac{1}{n^{|S|}} =\sum_{j=1}^{|S|}a_jF(1)\frac{1}{n^{|S|-j}}
$$
因为 $F(1)=1$，因此得到
$$
G(1)=\sum_{j=1}^{|S|}a_jn^j
$$
这样就做完了。

[代码](https://gitee.com/sshwy/code/raw/master/luogu/4548.cpp)

## SDOI2017 硬币游戏

设 $F_i(x)=\sum_{j=0}^\infty f(i,j)x^j$，其中 $f(i,j)$ 表示在扔第 $j$ 次后以 $S_i$ 结尾，结束扔硬币的概率。

设 $G(x)=\sum_{j=0}^\infty g(j)x^j$，其中 $g(j)$ 表示在扔第 $j$ 次后仍未结束的概率。

我们要求的是 $F_i(1),i\in[1,n]$。

类似地，首先有一个显然的等式：
$$
G(x)+\sum_{i=1}^nF_i(x)=G(x)x+1
$$
类似地，设 $a_{i,j,k}$ 表示 $S_i[1,k]$ 与 $S_j[m-k+1,m]$ 是否匹配。那么我们强行在当前状态下插入 $S_i$，可以得到
$$
G(x)\left(\frac{x}{2}\right)^{|S_i|}=\sum_{j=1}^n F_j(x)\sum_{k=1}^m a_{i,j,k} \left(\frac{x}{2}\right)^{|S_i|-k}
$$
相当于你在插入的过程中，可能在中途就以 $S_j$ 结尾结束了。

根据第一个等式，取 $x=1$ 得到
$$
\sum_{i=1}^nF_i(1)=1
$$
第二个等式取 $x=1$ 得到
$$
G(1)=\sum_{j=1}^nF_j(1) \sum_{k=1}^m 2^k a_{i,j,k}
$$
那么对于 $i\in[1,n]$ 再加上上面那个式子，我们可以 $O(n^3)$ 高斯消元求出答案了。

注：eps 要开到 $10^{-100}$。

[代码](https://gitee.com/sshwy/code/raw/master/luogu/3706.cpp)

## [PKUWC2018 随机游走](https://loj.ac/problem/2542)

首先考虑 min-max 容斥。设 $x_i$ 表示走到 $i$ 的时间的期望。那么我们要求的是 $\max x_i$，转化为求 $\min x_i$，即到达任意一个点的时间的期望。

这个可以用树上消元在 $O(n)$ 的复杂度计算，答案就是根结点的期望。

最后容斥的时候可以使用 FMT，把容斥系数带进去即可。

总时间复杂度 $O(2^nn)$。


[代码](https://gitee.com/sshwy/code/raw/master/loj/2542.cpp)
