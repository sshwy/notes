---
title: 长链剖分学习笔记
date: 2019-11-25 18:35:15
tags:
  - Notes
---

## 回顾树链剖分

树链剖分时，每个内部结点都会选择一个儿子做为自己链上的儿子，我们称为**继承儿子**（重儿子）。如果一个结点没有被继承，那么它就是一个**链顶结点**。一棵树剖分过后链的个数是 $\Theta(Leaf)$ 的，其中 $Leaf$ 表示叶子结点的个数。

按照树剖后的顺序先遍历继承儿子，然后遍历其他儿子，那么每条链上的结点的 DFN 序是连续的。这样可以用一个指针数组来模拟二维数组，于是就可以方便地进行一些 DP 转移。

## 高度

长链剖分的划分依据。设 $h(u)$ 表示 $u$ 的**高度**，则

$$
h(u)=\max_{v\in Son(u)}h(v)+1
$$

形式化地，长链剖分中每个内部结点选择的是其儿子中高度最大的点作为继承结点，设其为 $R_u$。

关于高度的性质：$u$ 所在的链长度**至少**为 $h(u)$。

长链剖分，是将高度最高的儿子结点作为继承儿子的一种剖分。在 dfs 的过程中对于每个结点 $u$，如果我们只遍历子树中除了子树 $R_u$ 之外的其他子树，那么复杂度是 $O(n)$ 的。设 $S(u)$ 表示子树 $u$ 的大小设 $P_u$ 表示 $u$ 所在链的链顶结点，$F_u$ 表示 $u$ 的父亲，形式化地：
$$
\sum_{u\in T}S(u)-S(R_u)=O(n)
$$
略证：考虑每个结点 $u$ 被遍历的次数。显然 $u$ 只会在 DFS $F_{P_u}$ 的时候被遍历一次。因此复杂度 $O(n)$。

注意区分**深度**：$dep(u)=dep(F_u)+1$。

## k 级祖先

> 给一棵 $n$ 度有根树，$q$ 次询问点 $x$ 的 $k$ 级祖先。

这题可以容易地 $O(n)-O(n)$、$O(n\log_2n)-O(\log_2n)$、$O(n\sqrt{n})-O(\sqrt{n})$ 来做。考虑长链剖分。

首先我们倍增求出 $f(u,j)$ 表示 $u$ 的 $2^j$ 级祖先。另外，对于每个**链顶结点**$u$，我们预处理出 $u$ 的前 $h(u)$ 级祖先和 $u$ 这条链上的结点序列。

则我们先求出一个最大的 $j$ 使得 $2^j\le k$。设 $u'=f(u,j),k'=k-2^j$，那么我们要求的就是 $u'$ 的 $k'$ 级祖先。

由于 $h(u')\ge 2^j>k'$，因此 $u'$ 的 $k'$ 级祖先要么在 $u'$ 所在的链上，要么在 $u'$ 所在链的链顶结点的祖先序列上。两者都可以 $O(1)$ 查询。因此分类讨论一下即可 $O(1)$ 查询 $k$ 级祖先。

时间复杂度 $O(n\log_2n)-O(1)$。

[代码](https://gitee.com/sshwy/code/raw/master/vijos_kth_ancestor.cpp)

## CF1009F

> 有根树。设 $d(u)$ 表示一个序列，$d(u,j)$ 表示在子树 $u$ 中与 $u$ 距离为 $j$ 的点的个数。对于每个 $u$ 询问 $d(u)$ 中最大值出现第一次的下标。

这题显然可以启发式合并。我们考虑长链剖分。

容易发现 $d(u)$ 的长度为 $h(u)$。考虑 $d(u)$ 继承自 $d(R_u)$。则显然有
$$
d(u,i+1)\gets d(R_u,i)\\
d(u,0)\gets 1
$$
然后我们遍历 $u$ 的非继承儿子 $v$，把这些儿子的序列贡献到 $d(u)$ 上：
$$
d(u,i+1)\gets d(v,i)
$$
这样就求出了 $d(u)$。然后在贡献和继承的时候顺便更新最大值即可，这样复杂度就是遍历非 $R_u$ 之外的结点的复杂度，因此总复杂度 $O(n)$。使用 deque 实现，支持在前端插入一个数和随机访问内存，实现起来方便。（只要不遍历长链，复杂度就是对的）

模板，放出核心代码

```cpp
deque<int> *d[N];
long long ans;

int dep[N],hgh[N];
int a[N],b[N];
void dfs1(int u,int p){
    dep[u]=dep[p]+1;
    hgh[u]=1;
    FORe(i,u,v)if(v!=p)dfs1(v,u),hgh[u]=max(hgh[u],hgh[v]+1);
}
void dfs2(int u,int p){
    int mx=0,son=-1;
    FORe(i,u,v)if(v!=p&&hgh[v]>mx)mx=hgh[v],son=v;
    if(son==-1){
        d[u]=new deque<int>;
        d[u]->pb(1);
        a[u]=0,b[u]=1;
        return;
    }
    dfs2(son,u);
    FORe(i,u,v)if(v!=p&&v!=son)dfs2(v,u);
    // 继承 son
    d[u]=d[son];
    d[u]->push_front(1);
    a[u]=0,b[u]=1;
    if(b[son]>b[u])b[u]=b[son],a[u]=a[son]+1;
    else if(b[son]==b[u])a[u]=min(a[u],a[son]+1);
    // 合并其他儿子的答案
    FORe(i,u,v){
        if(v==p||v==son)continue;
        int lim=(int)d[v]->size()-1;
        int mx2=0,ans=-1;
        FOR(i,0,lim){
            (*d[u])[i+1]+=(*d[v])[i];
            if((*d[u])[i+1]>mx2)mx2=(*d[u])[i+1],ans=i+1;
        }
        if(mx2>b[u])b[u]=mx2,a[u]=ans;
        else if(mx2==b[u])a[u]=min(a[u],ans);
        delete(d[v]);
    }
}
```

[完整代码](https://gitee.com/sshwy/code/raw/master/codeforces/1009f.cpp)

## Luogu3899

考虑长链剖分。设 $f(u,i)$ 表示子树 $u$ 内与 $u$ 距离为 $i$ 的结点的子树大小减 1 的和：
$$
f(u,i)=\sum_{v\in T_u,dep(u)+i=dep(v)}S(v)-1
$$


则分两种情况讨论：

1. $b$ 是 $a$ 的祖先，则方案数为 $\min(dep(a)-1,k)(S(a)-1)$；
2. $a$ 是 $b$ 的祖先，则方案数为 $\sum_{i=1}^{\min(k,h(a)-1)}f(u,i)$。

在继承的时候
$$
f(u,i+1)\gets f(R_u,i)\\
f(u,0)\gets S(u)-1
$$
在贡献的时候
$$
f(u,i+1)\gets f(v,i)
$$
考虑到我们询问的时候是一个区间和的形式，因此把 $f(u)$ 做后缀和处理即可。这样可以 $O(1)$ 计算询问，也可以 $O(1)$ 转移，复杂度 $O(n)$。

这是指针版的写法，感觉更好用：

```cpp
long long BIN[N],lb;
long long *f[N];

long long ans[N];
int hgh[N],dep[N],sz[N];
vector< pair<int,int> > qry[N];

void dfs1(int u,int p){
    hgh[u]=1,dep[u]=dep[p]+1,sz[u]=1;
    FORe(i,u,v)if(v!=p)dfs1(v,u),sz[u]+=sz[v],hgh[u]=max(hgh[u],hgh[v]+1);
}
void dfs2(int u,int p){
    ++lb;
    int mx=0,son=-1;
    FORe(i,u,v)if(v!=p&&hgh[v]>mx)mx=hgh[v],son=v;
    if(son==-1){
        f[u]=BIN+lb;// 指针赋值
        f[u][0]=sz[u]-1;
        return;
    }
    dfs2(son,u);
    FORe(i,u,v)if(v!=p&&v!=son)dfs2(v,u);

    f[u]=f[son]-1;
    f[u][0]=sz[u]-1+f[u][1];// 后缀和

    FORe(i,u,v){
        if(v==p||v==son)continue;
        FOR(i,0,hgh[v]-1)f[u][i+1]+=f[v][i];
        f[u][0]+=f[v][0];// 后缀和
    }
    // 处理询问
    for(pii p:qry[u]){
        int id=p.fi,k=p.se;
        ans[id]+=1ll*min(dep[u]-1,k)*(sz[u]-1);
        ans[id]+=f[u][1]-(k+1>=hgh[u]?0:f[u][k+1]);
    }
}
```

[完整代码](https://gitee.com/sshwy/code/raw/master/luogu/3899.cpp)

## BZOJ3252

把一个点拆成其权值长度的链。

然后在新的树上长链剖分后选前 k 大的链即可。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/3252.cpp)

## BZOJ4543

> 一棵树，求三个互不相同的点并且两两距离相等的方案数。

记 $f(u,i)$ 表示子树 $u$ 中与 $u$ 距离 $i$ 的点的个数，$g(u,i)$ 表示子树 $u$ 中的二元组 $(x,y)$ 的个数，满足 $x,y$ 到其 LCA 的距离都为 $d$ 且这个 LCA 到 $u$ 的距离为 $d-i$。即距离差为 $i$ 的二元组的个数。

考虑继承
$$
f(u,i)\gets f(R_u,i-1)\\
f(u,0)\gets 1\\
g(u,i)\gets g(R_u,i+1)
$$
考虑贡献
$$
f(u,i+1)\gets f(v,i)\\
g(u,i)\gets g(v,i+1)\\
g(u,i)\gets f(u,i)f(v,i-1)
$$
这里的 $f(u),g(u)$ 是指在合并完了 $v$ 之前的子节点的 DP 值。

考虑统计答案，我们可以在上述贡献的**过程**中统计：
$$
Ans\gets f(u,i-1)g(u,i)\\
Ans\gets f(v,i)g(u,i+1)\\
$$
在计算完 $f(u),g(u)$ 之后，还有 $Ans\gets g(u,0)$。

注意这些转移的边界，以及他们的转移顺序。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/4543.cpp)

## BZOJ1758

首先二分答案，则边权变成 $w-mid$，问题转化为是否存在一条长度在 $[L,U]$ 的路径权值非负。

如法炮制，设 $f(u,i)$ 表示 $u$ 的子树内以 $u$ 为端点的长度为 $i$ 的路径的权值最大值。

继承
$$
f(u,i)\gets f(R_u,i)+w(R_u)-mid\\
f(u,1)\gets w(R_u)-mid
$$
贡献
$$
f(u,i+1)\gets f(v,i)+w(v)-mid\\
f(u,1)\gets w(v)-mid
$$
这里的转移是取 $\max$。在合并的时候顺便检查是否存在 $x,y$ 使得 $f(u,x)+f(v,y)+w(v)-mid>0$，则我们枚举 $y$ 的时候查询区间最大值即可。因此使用线段树维护上述转移，需要支持区间加、单点对某个数取 $\max$、询问区间 $\max$。

时间复杂度 $O(n\log_2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/bzoj/1758.cpp)

