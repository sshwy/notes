---
title: KMP 进阶
date: 2020-02-04 17:36:54
tags:
---

## 约定

1. $S,T$通常表示一个字符串。
2. $|S|$表示$S$的长度。
3. $S[i]$表示$S$的第$i$个字符，$S[l,r]$表示$S[l],S[l+1],\cdots,S[r]$构成的子串。
4. 记$\nu(i)$表示$i$的fail指针。
5. $S^k$表示$k$个$S$拼在一起。

## Border

在进阶之前先铺垫一个基础概念——Border。KMP的fail指针指向的是它的一个最长的Border。

如果$|S|$被$(|S|-\nu(|S|))$整除，那么$S$就是一个循环串。

对于一个串$S$，如果它的最长Border的长度为$L=\nu(|S|)$，且$L\ge \frac{|S|}{2}$，那么$S$就可以写成$AB^k$的形式，且$|B|=|S|-L$，$A$是$B$的后缀。此时最长的Border是$AB^{k-1}$。这种情况下的fail链是$AB^{k},AB^{k-1},\cdots,AB$。相当于每一次跳一个$B$。

## Rank KMP

> For sequence $A$ and $B$ length of $n$, we say $A$ similar to $B$, IFF for all $i$ and $j$, $A_i<A_j \Leftrightarrow B_i<B_j$.
>
> Now you have two sequence $X$ and $Y$, consider the number of consistent subsequence of $X$ that similar to $Y$.
>
> $|X|,|Y|\le 10^6$.

与KMP算法类似。在扩展的时候，KMP是判断两个字符是否相等。这里我们则改为判断两个字符在前面的排名是否相同。

主席树即可。

时间复杂度$O(n\log_2n)$。

## 树上KMP

> 给一个Trie，求每个点到根的路径表示的字符串的最长Border。
>
> $N,|\Sigma|\le 10^6$。

对于$u$的父节点$p_u$，假设到$p_u$的串表示为$AB^k$，那么每次跳fail匹配的是同一个字符$B[1]$。因此如果$B[1]$不匹配就可以直接跳到$AB$了。如果不是$AB^k$就正常跳fail。这样每次至少减少一半，复杂度$O(n\log_2n)$。



