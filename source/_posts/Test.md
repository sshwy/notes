---
title: 测试页面
date: 2020-04-12 11:49:15
tags:
---

## H2 再强没有

> 引用一段文字 Content

### H3 再高没有

一段文字 Content

#### H4 dyls眉头一皱

一段文字 Content

##### H5 冷冰冰

一段文字 Content

###### H6 自闭了

*斜体* **粗体** ~~删除~~ `inline code` {% label @标签 %} [链接](https://sshwy.name) $a^2+b^2=c^2$.
$$
x=\frac{-b\pm \sqrt{b^2-4ac}}{2a}
$$

```cpp
#include<bits/stdc++.h>
using namespace std;
int a,b;
int main(){
	cin>>a>>b;
    cout<<a+b<<endl;
    return 0;
}
```

- 无
- 序
- 列
- 表

1. 有
2. 序
3. 列
4. 表

| 表格                                          | 平时       | 比赛               |
| --------------------------------------------- | ---------- | ------------------ |
| gal $\textbf{b}{\color{red}\text{lunt-axe}}$  | 假         | AK；或者不屑于参赛 |
| xd $\textbf{x}{\color{red}\text{ryjr233}}$    | 假         | AK                 |
| dyls $\textbf{d}{\color{red}\text{ysyn1314}}$ | 假；是妹子 | AK                 |

