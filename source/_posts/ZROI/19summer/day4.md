---
title: AB 班模拟赛 Day4
date: 2019-08-07 10:37:28
tags:
---

## A

斜率优化 DP。记 $a_i$ 表示接第 $i$ 只猫的最早出发时间。显然 $a_i=t_i-\displaystyle\sum_{j=2}^{h_i}d_i$。我们把 $a_i$ 排序，那么一个人肯定会选择一段连续的区间取猫。设 $A_i=\displaystyle\sum_{j=1}^ia_j$，$f_{i,j}$ 表示接前 $i$ 只猫用 $j$ 个人的最小代价。
$$
f_{i,j}=\min_{k=0}^{i-1}\{f_{k,j-1}+a_i(i-k)-(A_i-A_k)\}
$$
然后斜率优化 DP 即可。复杂度 $O(mp)$。

## B

乱搞题，不太想记录。

## C

高精度开根，不会。
