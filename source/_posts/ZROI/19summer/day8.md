---
title: AB 班模拟赛 Day8
date: 2019-08-11 10:37:28
tags:
---

## A

考虑 10 分做法：每一个排列一步到位。

我们考虑要交换 u,v 就把 1 号位置当作临时转存，即各自和 1 换，三步实现，这样总步数是 $O(3n)$ 的。

考虑 $A=2$。构造一个 $2-n$ 的循环左移 $1$ 位的排列，构造 $1,2$ 位置的交换。如果我们要换 $u,v$ ，那么通过循环左移把 $u$ 移到 $2$ 的位置，然后交换 $1,2$ ，然后把 $v$ 移到 $2$ 的位置，然后交换 $1,2$ ，再把 $u$ 移到 $2$ 的位置，交换一下：

$$
\{1,3,4,5,\cdots,n-1,n,2\}\\
\{2,1,3,4,\cdots,n-1,n\}
$$

小细节：右边是 $2,3,4\dots,n$ ，那么我们每次把 $1$ 位置上的数放到正确的相对位置上去。如果是 $1$ 的话就找一个第一个位置不对的数字放到 $1$，然后继续用该算法做。

然后加一个倍增，分别构造左移 $1,3,8,20$ 位的排列（数字通过调参得到），剩下一个的是 $1,2$ 交换的排列，然后构造方案即可。

## B

真 · 计数题。假设选出来的序列是 $\left\langle a_i \right\rangle_{i=1}^k$，则贡献为

$$
\begin{aligned}
&\frac{1}{k}\sum_{i=1}^k(a_i-\overline{a})^2\\
=&\frac{1}{k}\left(\sum_{i=1}^k a_i^2-2\overline{a}\sum_{i=1}^k a_i+k\overline{a}^2\right)\\
=&\frac{1}{k}\sum_{i=1}^k a_i^2-\frac{1}{k^2}\left(\sum_{i=1}^k a_i\right)^2\\
\end{aligned}
$$

在长度为 $n$ 的区间中选数，对于 $\dfrac{1}{k}\sum_{i=1}^k a_i^2$，可以计算每一个数出现的次数，显然它会在长度为 $k$ 的所有选出的可能的序列中出现 $\dbinom{n-1}{k-1}$ 次；  
对于 $\dfrac{1}{k^2}\left(\sum_{i=1}^k a_i\right)^2$ ，考虑将平方项拆开算贡献，则 $a_i^2$ 相当于出现了 $\dfrac{\binom{n-1}{k-1}}{k^2}$ 次；$a_ia_j$ 出现了 $\dfrac{\binom{n-2}{k-2}}{k^2}$ 次；  
最后还得减掉一个 $\dfrac{\binom{n-2}{k-2}}{k^2}a_i^2$（这种贡献是不合法的）。

于是问题转化为

$$
\sum_{i=1}^na_i^2\sum_{k=1}^n\frac{\binom{n-1}{k-1}}{k}-\sum_{i=1}^na_i^2\sum_{k=1}^n\frac{\binom{n-1}{k-1}}{k^2}-\sum_{i=1}^n\sum_{j=1}^na_ia_j\sum_{k=1}^n\frac{\binom{n-2}{k-2}}{k^2}+\sum_{i=1}^na_i^2\sum_{k=1}^n\frac{\binom{n-2}{k-2}}{k^2}\\
$$

这样就可以 $O(n^2)$ 算组合数暴力。

但我们发现组合数的式子只和序列长度有关。于是我们一个一个凑配。先化第一个式子：

$$
A_n=\sum_{k=1}^n\frac{\binom{n-1}{k-1}}{k}=\sum_{k=1}^n\frac{(n-1)!}{(k-1)!(n-k)!}\frac{1}{k}=\frac{1}{n}\sum_{k=1}^n\binom{n}{k}=\frac{1}{n}(2^n-1)
$$

第二个式子：

$$
B_n=\sum_{k=1}^n\frac{\binom{n-1}{k-1}}{k^2}=\frac{1}{n}\sum_{k=1}^n\frac{1}{k}\binom{n}{k}\\
$$

记

$$
P_n=\sum_{k=1}^n\frac{1}{k}\binom{n}{k}\\
B_n=\frac{1}{n}P_n
$$

差分一下得到

$$
\begin{aligned}
P_{n+1}-P_{n}=&\frac{1}{n+1}\binom{n+1}{n+1}+\sum_{k=1}^{n}\frac{1}{k}\left(\binom{n+1}{k}-\binom{n}{k}\right)\\
=&\frac{1}{n+1}+\sum_{k=1}^n\frac{1}{k}\binom{n}{k-1}\\
=&\frac{1}{n+1}+\frac{1}{n+1}\sum_{k=1}^n\binom{n+1}{k}\\
=&\frac{1}{n+1}(2^{n+1}-1)
\end{aligned}
$$

考虑第三个和式

$$
\begin{aligned}
C_n&=\sum_{k=2}^n\frac{1}{k^2}\binom{n-2}{k-2}=\sum_{k=2}^n\frac{(n-2)!}{(k-2)!(n-k)!}\frac{1}{k^2}\\
&=\frac{1}{n(n-1)}\sum_{k=2}^n\frac{k-1}{k}\binom{n}{k}\\
&=\frac{1}{n(n-1)}\sum_{k=1}^n\frac{k-1}{k}\binom{n}{k}\\
\end{aligned}
$$

设

$$
Q_{n}=\sum_{k=1}^n\frac{k-1}{k}\binom{n}{k}
$$

差分一下得到

$$
Q_{n+1}-Q_n=\frac{n}{n+1}+\sum_{k=1}^n\frac{k-1}{k}\left(\binom{n+1}{k}-\binom{n}{k}\right)\\
=\frac{n}{n+1}+\sum_{k=1}^n\left(1-\frac{1}{k}\right)\binom{n}{k-1}\\
=\frac{n}{n+1}+\sum_{k=0}^{n-1}\binom{n}{k}-\sum_{k=1}^n\frac{1}{k}\binom{n}{k-1}\\
=\frac{n}{n+1}+(2^n-1)-\frac{1}{n+1}(2^{n+1}-2)\\
$$

于是 $O(n)$ 地预处理 $\left\langle A_i \right\rangle,\left\langle B_i \right\rangle,\left\langle C_i \right\rangle$ ，然后线段树维护区间和、区间平方和就可以了。

## C

真真假假假假真真，不造怎么做。
