---
title: AB 班模拟赛 Day6
date: 2019-08-09 10:37:28
tags:
  - Complete-Solution
---

## A

这题可以哈希做。用一个 map 存每个哈希值出现的次数，用栈模拟整个过程。

但我考场的做法是一个类似 KMP 的过程。对于每一个字符，我们找到在它前面的，离它最近的与它相同的字符，并且两个字符之间的串是合法的括号序列。那么，一个合法的括号序列显然可以分成若干个连续的合法括号序列，并且这些序列不能再分割。于是统计答案的时候相当于段数的一个组合数。对每个点求一个 pre 指针，然后扫一遍统计答案即可。

## B

显然这题可以状圧 DP，如果我们知道了 m 个方案的任意子集方案的边集的并，那么设 $f_{i}$ 表示选择的公司的二进制状态是 $i$ 的最大收益，就可以轻松地完成 DP 的转移。我们把问题形式化：

令 $T\in [m]$，设 $E_i$ 表示第 i 个公司装修的边集，$S_i$ 表示第 $i$ 个公司装修的点集，我们要求的是

$$
f(T)=\left|\bigcup_{i\in T}E_i\right|
$$

容易想到容斥：

$$
f(T)=\left|\bigcup_{i\in T}E_i\right|=\sum_{P\subseteq T}(-1)^{|P|-1}\left|\bigcap_{i\in P}E_i\right|
$$

由于边的交集只和点的交集有关，我们可以得到

$$
g(P)=\left|\bigcap_{i\in P}E_i\right|=\binom{|\bigcap_{i\in P}S_i|}{2}\\
f(T)=\sum_{P\subseteq T}(-1)^{|P|-1}g(P)
$$

问题转化为求一个 $g'(T)=|\bigcap_{i\in T}S_i|,\;T\in [m]$。

可以 FMT 求解。具体地，我们考虑每一个点 $u$ 的贡献。一个点会出现在若干个公司的计划中，我们找到这些公司，他们组成了一个集合 $T_u$。那么，包含这个点的交集 $T$ 显然是 $T_u$ 的子集。因此做一次子集后缀和即可。一开始可能会考虑算重的问题，但把 FMT 想清楚：它是子集后缀和，不是超集的和！

求了 $g'(S)$ 就可以求 $g(S)$。然后对于上述容斥的式子，我们可以变换一下得到

$$
f(T)=\sum_{P\subseteq T}(-1)^{|P|-1}g(P)
$$

设 $g''(T)=(-1)^{|T|-1}g(T)$，显然我们得到

$$
f(T)=\sum_{P\subseteq T}g''(P)
$$

后者就是一个子集前缀和，可以 FMT 求解。

## C

我们把排列理解为环：对于 $\left\langle p_i\right\rangle_{i=1}^n$，$p_i$ 指向 $i$。这样排列就是若干个互不相交的简单环。

考虑交换两个数的含义，相当于把它们指向的位置交换。这样就导致两种结果：两个环连在一起；或者一个环分成两个。

对于一个环上有异色的点，我们总可以在环长 -1 步内把它消成自环。考虑 $x\to y\to z$，把 $x$ 和 $z$ 交换，则 $y$ 相当从环中被删除了。搞一个好的消点方案可以把这个环消掉。

现在就只剩下了若干个纯色的环，可以按颜色分类。但环之间的颜色可能是不一样的。因此我们可以贪心地匹配两个颜色不同的环，用一次操作把它们连在一起，然后又可以在环长 -1 补内消掉这个大环。匹配的时候我们贪心地先匹配个数多的一类环，可能每种颜色要先预留一个，然后最后每一类只剩下 1 到 2 个环，搞一些高妙的方法配对一下就行。

最后，会剩下若干个同色的纯色环。那么我们把他们连到一起，然后找一个异色的自环连进去，就可以消掉了。

考场上写的时候，贪心策略有 BUG，但也 A 掉了。
