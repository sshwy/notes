---
title: AB 班模拟赛 Day9
date: 2019-08-12 10:37:28
tags:
---

## A

f[x,k,O(1)]表示x的子树有k条链的最大答案。

1. 不包含x
2. 含x，但是直链
3. 含x，是弯的

转移：合并子树。

1. x,y都是直链：
   1. y这边截住，x的保留
   2. 并在一起，产生一条弯的链
2. x是直链，y是……

```text
dfs(x):
	for y in Son(x)
		dfs(y)
	f[x,k,0..2]
	init while only a node x
	tmp[x]=merge(f[x],f[y]
	f[x]=tmp[x]

merge:
    for i=0 to min(sz[x],k)
        for j=0 to min(sz[y],k)
            f[x][i] ... f[y][j]
```

输出方案：记录每一个状态是怎么来的
记录每一次合并的额外信息。

## B

O(nQ) 的做法

$$
\sum_{i=l}^r\sum_{j=i}^rf(i,j)
$$

假设询问区间为 $[x,y]$ ，而当前线段树上的区间为 $[l,r]$，那么$[l,r]$这个点被调用的次数可以分为：

1. $[x,y]$是$[l,r]$的子区间；
2. $[x,y]$与$[l,r]$相交；
3. $[x,y]$包含$[l,r]$但不包含它的父亲。

对于一个询问 $[L,R]$，设$[l,r]\subseteq[L,R]$，我们分类计算贡献。
$$
\begin{aligned}
g_1(l,r)=&\binom{r-l+1}{2}=\frac{(r-l+1)(r-l)}{2} \\
g_2(l,r)=&(l-L)(r-l+1)+(r-l+1)(R-r) \\
=&(r-l+1)(R-L+l-r) \\
=&(r-l+1)(l-r)+(r-l+1)(R-L) \\
g_3(l,r)=&\begin{cases}
(l-l')(R-r) =R(l-l')-r(l-l') & [l,r]\subseteq [l',r] \\
(l-L)(r'-r) =l(r'-r)-L(r'-r) & [l,r]\subseteq [l,r']
\end{cases}
\end{aligned}
$$
合并上述三项得到
$$
m=\frac{l+r}{2}\\
=\frac{(m-l+1)(m-l)}{2}+(m-l+1)(l-m)+(m-l+1)(R-L)+l(r-m)-L(r-m)+\\
\frac{(r-m)(r-m-1)}{2}+(r-m)(m+1-r)+(r-m)(R-L)+R(m+1-l)-r(m+1-l)\\
=\frac{l-l^2+r-r^2}{2}-m^2-m+2lr+R(r+m-2l+2)+L(m-2r+l-1)
$$


于是我们会对线段树上每个这样的点计算贡献。直接这样做，每一次询问的复杂度是$O(n)$的。
