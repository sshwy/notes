---
title: ZROI2019 CSP-S 十连测 Day8
date: 2019-10-29 19:04:09
tags:
  - Complete-Solution
---

## A

欧拉公式。

## B

考虑状圧 DP。设 $f(i,mask)$ 表示考虑融合串长度为 $i$，$mask$ 第 $j$ 位为 $1$ 表示这个串可以由 $s$ 的前 $i-j$ 位和 $t$ 的前 $j$ 位融合而成，的方案数。

转移的时候枚举融合串第 $i+1$ 位上的数字，并且判断 $s$ 的第 $i-j+1$ 位是不是这个字符或者 $t$ 的第 $j+1$ 位是不是这个字符。两种情况分别代表第 $i+1$ 属于 $s$ 或者 $t$。然后并起来就是新的 $mask'$。这个东西可以预处理一些状态。

假设字符串 $0$ 起点。设
$$
g_s(i,bit)=\sum_{j=0}^i[s_{i-j}=bit]2^j\\
g_t(i,bit)=\sum_{j=0}^i[t_j=bit]2^j
$$
其中 $bit=0/1$。这样可以得到 $mask'=(g_s(i,bit)\operatorname{bitand} mask)\operatorname{bitor} 2\cdot (g_t(i,bit)\operatorname{bitor} mask)$。

初状态 $f(0,2^{n+m}-1)=1$，答案 $f(n+m,2^m)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/2019TG/8/b.cpp)

## C

大分类讨论的题，分对角线和不对角线（或者没有贡献）的情况，做横竖斜线加和矩阵加即可。差分维护，最后再搞回来。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/2019TG/8/c.cpp)
