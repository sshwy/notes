---
title: 广州集训-杂题选讲
date: 2019-07-29 00:00:00
tag:
---

## CEOI2016 Kangaroo

> 求有多少个 $n$ 阶排列 $P$ 满足 $P_1=s,P_2=t$ ，且
>
> $$
> \forall i\in[2,n-1],\ (P_{i}<P_{i+1}\wedge P_{i}<P_{i-1})\vee (P_{i}>P_{i+1}\wedge P_{i}>P_{i-1})
> $$
>
> $1\le s\neq t\le n\le 2000$。

维护一个链的集合。我们从小到大枚举每个数。我们可以把这个数本身当作一个链插入到某个位置，也可以连接两个相邻链。注意我们是从小到大枚举数，因此连接两个链的时候保证左右两边的数小于当前数。这就可以 DP。$f[i,j,k]$ 表示不考虑 s,t，已经填了 $1\sim i$，组成了 $j$ 条链，$s,t$ 的连接状态为 $k$ 的方案数。初始时 $f[2,2]=1$。目标 $f[n,1]$。

记录 k 的目的是考虑能否连接 s 或 t。转移就不写了，记个数而已。主要是分类讨论麻烦。看 [Cydiater 的题解](https://cydiater.me/2017/10/09/%E3%80%8CBZOJ4934%E3%80%8D%E3%80%8CCEOI2016%E3%80%8DKangaroo/) 吧。

## 画仙人掌

> 给一个 n 个点的仙人掌，要求给每个点分配一个坐标 $(x,y)$，使得在平面上边不相交。x,y 是 1 到 n 的整数。$n\le 10^5$.

先考虑一棵树的情况，那么我们让每个点的 x 坐标为深度，y 坐标为 dfs 序。这样可以将树分层，每一层的儿子结点纵坐标是递增的，边不会相交。

仙人掌本身是具有平面图性质的。我们考虑给仙人掌建圆方树。然后仍让点的横坐标为深度。因为只有在一个环上才会建方点，而环的边数至少为 3，因此圆方树的深度不会超 n。y 坐标仍是 dfs 序，但是是修改版，在 dfs 时使得这个点的第一个儿子的 dfs 序等于它自己，这样即使增加了方点，dfs 序也不超过 n。还有一种方法是直接把方点的 dfs 序写成一个实数。由于方点和圆点是交替出现的，因此不会无限分割值域。

## AGC030F

> 有一个长度为 2N 的序列 $A$，$A_i\in \{-1,1,2,\cdots,2N\}$。非 -1 的数在 A 中至多出现 1 次。现在要将所有 -1 替换为 1 到 2N 中的一个数，使得 A 变成排列。
>
> 定义 $n$ 阶序列 $B$，其中 $B_i=\min(A_{2i-1},A_{2i})$。求不同的 B 序列的个数。对 $10^9+7$ 取模。$n\le 300$。

考虑一对 pair$(A_{2i-1},A_{2i})$，如果两个的值都固定的就不用管了，剩下两种情况：$(-1,x),(-1,-1)$。对于 $(-1,-1)$ 可以忽略他们的顺序，算出答案后乘上个数的阶乘。我们从大到小枚举数字，$f[i,j,k]$ 表示枚举到 i，有 j 个 -1 和 k 个 x 没有匹配（这里是指枚举到的，没有匹配的。不包括那些一开始就不匹配的）。

如果 i 是某个 x，可以考虑：

1. 不匹配，转移到 $f[i-1,j,k+1]$。
2. 匹配一个 -1，转移到 $f[i-1,j-1,k]$。

如果 i 不是某个 x，可以考虑：

1. 不匹配，转移到 $f[i-1,j+1,k]$。
2. 匹配一个 -1，转移到 $f[i-1,j-1,k]$。
3. 匹配一个 x，有 k 种方案。转移到 $f[i-1,j,k-1]$。

## 传送门

> 一个 n 个点 m 条边加权无向图。你要从 S 走到 T，但有一条边是坏的，而你只有在站在它的某个端点上的时候才知道它是坏的。问你从 S 点走到 T 点最坏情况下的最短距离。

先求以 T 为根的最短路树，显然一开始你会沿着树边走。如果删的不是树边，就走最短路，否则删了树边，那么为了到达 S，你会先沿着树边另一些走到某个结点，然后走一次非树边，然后继续走树边到 T。假设你走到 u，然后往上走的树边被删了，那么你会想退到 u 的某个子树结点 v 上，然后走一个非树边走到 w，然后从 w 走树边到 T。于是除了 S 到 u 之外的代价为
$$
d[v]-d[u]+d[w]+dist(v,w)
$$
其中 d 表示最短路长度。于是对于结点 u 可以求出从 u“跳出去”的最小代价。方法就是先求 u 的子节点的信息，用堆维护，然后把他们合并到 u 上。注意，那些跳到 u 上的决策就是不可行的，要删掉，于是可以用可并堆或者启发式合并做。

最后用 DP 求最坏情况下的最小值即可。令 $f[u]$ 表示 S=u 时的方案。$d[u],e[u]$ 分别表示 u 到 T 的最短路，删树边后 u 到 T 的最短路。从 T 开始往前转移，枚举最坏情况，然后取最小值：
$$
f[v]=\min\left\{\max\left(f[u]+dist(u,v),\left\{\begin{aligned}&e[v],\text{u is v's parent}\\&d[v],\text{u isn't v's parent}\end{aligned}\right.\right)\right\}
$$

## LOJ6130

> 询问冒泡排序执行 k 轮后的序列。

冒泡排序：对于所有前面有比它大的数，一轮后把它往前移一个。对每一个数考虑，如果前面有 k 个比它大的数，就前移 k 位，否则相当于移动到比它小的数的个数的位置。于是用线段树或者树状数组统计，就可以确定每个数最终的位置，于是就没了。

## ARC073F

> 你有两个数 a 和 b。每次给你一个 $x_i$，你选择两个数之一 y 变成 $x_i$，代价是 $|x_i-y|$。求做完所有操作的最小代价。
>
> $n,x,a,b\le 2\times 10^5$。

$f[i,j]$ 表示两个数分别为 $x_i,x_j$ 的情况。限制 $i>j$。于是转移如下
$$
f[i,j]\xrightarrow{|x_i-x_{i+1}|} f[i+1,j]\\
f[i,j]\xrightarrow{|x_{i+1}-x_j|} f[i+1,i]
$$


容易发现，第一种转移相当于整个数组都增加 $|x_i-x_{i+1}|$。而第二种转移则是 $\forall j\in[1,i-1],f[i,j]$ 转移到 $f[i+1,i]$，相当于区间取最小值的操作。于是我们先对 $f[i]$ 计算区间最小值，然后把数组整体加 $|x_i-x_{i+1}|$。然后再拿刚求的最小值更新 $f[i+1,i]$。事实上由于 $j<i$，因此 $f[i+1,i]$ 是没有初始值的，可以直接赋值为刚求的最小值。最后的答案是 $f[n]$ 的最小值，于是我们的操作转化为

1. 区间加操作
2. 单点修改操作
3. 查询区间最小值

把第一维省掉，线段树维护即可。

## Snuke the Phantom Thief

> 二维平面上有 n 个宝石，位置为 $(x,y)$ 价值为 w 。有 m 个限制，每个限制形如在一条与 x 轴或 y 轴平行的直线下方（or 上左右）最多只能取 x 个宝石。问最多能取的宝石的价值是多少？

每个点用二元组表示，而直线相当于分别对 x 和 y 做限制。因此把每个 $(x,y)$ 拆成 x 和 y，在 x 和 y 连边。把 x，y 分别单调地排序，那么连出的图大概长这样。

![p1](https://hexo-source-1257756441.cos.ap-chengdu.myqcloud.com/2019/07/06/140745.png)

中间的边容量为 1，而直线相当于就是两边的边的流的上下界。于是这就是一个上下界费用流问题。上下界费用流可以把一条边拆成两条：$(R-L,w),(L,INF)$，这样就满足了上下界，而这样必须要求图没有费用的负圈（除非负圈费用算数）。

## 简单数据结构题

> 给一棵 n 个点的树，点权开始为 0，有 q 次操作，每次操作是选择一个点，把树上到它距离为 1 的点点权 +1，并输出每次操作后这些点点权的异或和。$n,q\le 5\times 10^5$。

加一操作在二进制位上是什么过程？从末位开始往前把连续的 1 变成 0，直到遇到一个 0 就把这个 0 变成 1，结束过程。因此就有
$$
x\oplus (x+1)=2\operatorname{zerobit}(x)-1
$$

其中 zerobit 表示 x 的最低的 0 位。因此我们的问题转化为了对每个点的儿子集合维护一个结构，支持

1. 单点加 1
2. 整体加 1
3. 查询全局异或和

而有了 zerobit 的想法，我们的问题转化为了单点异或，整体异或和查询异或和。当前点的权值是可以快速求出的。每个操作只在父节点上做直接修改，在自己身上打 Tag。查询当前点的权值只需要结合父节点的 Tag 就行。整体异或的时候，利用 zerobit，我们只需要在知道集合中 zerobit 为每个位分别有几个即可。

考虑使用 TRIE 维护集合，把数倒过来插入到 TRIE 中。即根结点是最低位。考虑全局 +1 的过程。如果最低位是 0 就会变成 1，如果最低位是 1 就会变成 0 并进位。这就是交换左右子树，然后递归左子树即可。查询 zerobit 的个数直接在 TRIE 上走 0 边就行。这样就做了整体异或。

单点异或我们直接做插入删除就行。

复杂度 $O(n\log_2n)$。

## 染色游戏

> 给一个 n 阶序列，选出一个上升子序列使得 $\sum a-\sum\frac{b(b+1)}{2}$ 最大。其中 a 是所有被选中的数，b 是所有未选择连续段的长度。$n\le 10^6$。

定义 $f[i]$ 表示前 i 个数以选择 $a_i$ 结尾的最大价值。
$$
f[i]=\max_{0\le j<i,a_j<a_i}\left\{f[j]+a_i-\frac{(i-j)(i-j-1)}{2}\right\}
$$
显然，如果没有上升子序列的条件它就是一个斜率。而现在决策受到限制。考虑分治优化 DP。我们按照权值分治。

我们先递归左边，完成子问题后，考虑利用左边的决策更新右边的决策。把左边的决策按权值排序，这样就没有 $a_j<a_i$ 的条件。由于左边的决策始终在右边决策之前，且右边的 DP 点单调的，因此可以斜率优化 DP 做到 $O(T)$ 的复杂度（T 是子问题规模）。更新完了再递归右边。排序的时候使用归并，可以做到 $O(T)$。于是总复杂度 $O(n\log_2n)$。

给一个通俗易懂的伪代码。当时上课时写的，感觉很好懂就保留了

```
CDQ 分治：
solve(l,r)：// 算 (l,r) 的 DP 值
	solve(l,mid)
	work(l,mid,mid+1,r) // 算贡献
	solve(mid+1,r)
```

## Func

> 已知
> $$
> \begin{aligned}
> &F(1)=1\\
> &F(2i)=F(i)\\
> &F(2i+1)=F(i)+F(i+1)
> \end{aligned}
> $$
> 给一个 n，求所有满足 $F(m)=n$ 的**正奇数**m。输出时对 $998244353$ 取模。

可以归纳得出，$F(n),F(n+1)$ 是互质的。并且对于 n 为偶数，$F(n+1)>F(n)$。

题目要求 m 是奇数，因此 $F(m-1)<n,F(m-1)\perp n$。可以枚举这个 $F(m-1)$，设枚举的数是 x。现在我们知道了 $n,x$，它们是这个函数两个相邻位置的值，可以倒推出 $F(\frac{m-1}{2})=x,F(\frac{m+1}{2})=n-x$。注意你知道 $n,x$ 但不知道 m。接下来你想要继续递归下去，但你不知道 $\frac{m-1}{2}$ 的奇偶性。喜闻乐见的是我们知道对于 n 为偶数，$F(n+1)>F(n)$。于是根据 $x,n-x$ 的大小关系判断奇偶性，然后可以递归下去。递归的边界是 $F(1),F(2)$ 或者 $F(2),F(3)$。然后递归回去就行。

考虑复杂度。的确是 $O(\log_2m)$ 的，但是你不知道 m 的值！你知道每次递归是 $n,x$ 辗转相减的过程，因此其实是 $O(n)$ 的。加上一开始的枚举，复杂度是 $O(n^2)$ 的。不过另一个显而易见的优化是把它变成辗转相除，即每次多路递归。可以做到 $O(n\log_2n)$.

结尾讲一下怎么归纳吧。为了证 $F(n)\perp F(n+1)$，分类讨论：

1. n 为偶数，则我们需要证 $F(\frac{n}{2})\perp (F(\frac{n}{2})+F(\frac{n}{2}+1))$，即 $F(\frac{n}{2})\perp F(\frac{n}{2}+1)$。
2. n 为奇数，则我们需要证明 $(F(\frac{n-1}{2})+F(\frac{n+1}{2}))\perp F(\frac{n+1}{2})$，即 $F(\frac{n-1}{2})\perp F(\frac{n+1}{2})$。

也就归纳完了。

## AGC001E

> 给 n 阶 A,B 序列，求
> $$
> \sum_{i=1}^n\sum_{j=1}^n\binom{A_i+A_j+B_i+B_j}{A_i+A_j}
> $$
> $n\le 2\times 10^5,1\le A_i,B_i\le 2000$。

推的过程就不喵了。一开始想的是
$$
\sum_{i=1}^n\sum_{j=1}^n(0,0)\to (A_i+A_j,B_i+B_j)\text{的路径条数}
$$
后来发现这样貌似不太能枚举 i,j，发现正解写的是
$$
\sum_{i=1}^n\sum_{j=1}^n(-A_i,-B_i)\to (A_j,B_j)\text{的路径条数}
$$
这样可以转化为从一个点集的任意一个点出发走到另一个点集的任意一个点的方案数和，点集的大小是 n。这可以直接二维 DP 解决。具体一下就是 $f[i,j]$ 表示起点点集任意一个点走到这个点的方案数，然后随便转移就行。由于 $A_i+A_j\le 4000$，$O(n^2)$ 可以过。

## cheese

> 有 n 对 pair $(v_i,m_i)$，你能最多分割两对 pair, 即将 $(v,m)$ 按比例分成 $(pv , pm)$ 和 $((1 − p)v , (1 − p)m)$，p 是你钦定的。分割后你希望将所有 pair 分成两组使得 v 之和相同且 m 之和相同。$n\le 10^5$。

脑筋急转弯。我们考虑一维的情况，就是给你一堆数，你可以把一个分割成两个，要让两个集合的和相同。于是你把数排成一排中间切一刀就喵了。

二维的情况，先算 v，m 的前缀和，在 xOy 上可以画成一个折线图。

![p2](https://hexo-source-1257756441.cos.ap-chengdu.myqcloud.com/2019/07/06/165953.png)

你发现这就是一个分段一次函数，而我们需要调整这个函数。切一刀相当于你的部分折线可以沿着某个斜率上下滑动，于是让它和对角线中点相交，然后我们在这个点再切一刀就没了。为了方便程序的过程实现，我们可以按斜率递增排序，每次把一段直线从左边移到右边，这样折线会不断靠近对角线中点。然后等离得足够近了选择一个在某方向上足够长的直线切，移动到中点再一切就行。还有一种策略是一个一个连接直线，如果跑到对角线上面去就就连接斜率小的直线，如果跑到对角线下面去了就连接斜率大的直线。连接完了发现一定可以找到中点附近的一个直线割掉来移动到中点。

怎么胡怎么来。好吧题解是这样的

> 把每个 pair 看成矩形 $\frac{m_i}{v_i}\times v_i$ , 总宽度为 $V =\sum v_i$，总面积为 $M=\sum m_i$。按照 $v_i$ 排序。问题变成选出一段宽为 $\frac{V}{2}$ 的使得面积为 $\frac{M}{2}$。二分答案即可。

很喵啊。

## 网友串

> 网友串：长度为偶数的非回文串。现在有 n 个长度分别为 $a_i$ 的 01 串，记为 $s_i$。定义第 i 个 01 串是混沌串的当且仅当存在 j<i 使得 $s_js_i$ 是网友串。问构造 n 个长度分别为 $a_i$ 的 01 串，且恰有 k 个混沌串的方案数。
>
> $1\le n\le 50,1\le a_i\le 7$.

圧状态。

首先奇数和偶数长度的串是不会连在一起的，因此可以分开算，最后答案做 k 的卷积就行。接着我们预处理每一对网友串能否构成混沌串。我们需要将方案按不同状态分类，因此用三元组 $(i,j,s)$ 表示前 i 个串出现 j 个混沌串，且这 i 个网友串能与集合 s 中的网友串组成混沌串的出现过至少一次。

然后就 DP 啊，$f[i,j,s]$ 表示到达这个状态的方案数。转移就瞎转。然后你发现可能出现的 s 很少。没了。

当 $a_i=1,3,5,7$，s 的个数是 238；当 $a_i=2,4,6$，s 的个数是 106。

## tree

> 给出一棵点带权有根树，在模 $10^9+7$ 意义下：
>
> 1. 询问路径权值方差
> 2. 修改路径上的点权 x 为 ax+b
> 3. 询问子树权值方差
> 4. 修改子树路径上的点权 x 为 ax+b
> 5. 询问毛毛虫上的权值方差
> 6. 修改毛毛虫上的点权 x 为 ax+b
>
> 毛毛虫：一个集合 $S(u,v)$：如果一条边的某个顶点在 u 到 v 的路径上, 那么这条边属于 S(u,v)。$n,q\le 10^5$。

方差就是和平方与平方和的线性计算。具体表示为
$$
S^2=\frac{\sum_{i=1}^n(x_i-\overline{x})^2}{n}
=\frac{\sum_{i=1}^nx_i^2}{n}-\overline{x}^2
$$
于是操作 1,2 就转化为了路径维护权值和、平方和。修改平方和的时候借助权值和完成。使用普通的树链剖分可以解决子树的维护，即 1-4 操作。这个毛毛虫是一个很棘手的东西。原题解的方法是在剖分的时候，先处理重链，然后把这个重链的毛毛虫的点分配连续的编号。保证每个重链的编号与毛毛虫的编号连续就能同时做子树和毛毛虫的情况了。但这个好像是假的。感觉为什么要把毛毛虫的编号分成连续的啊...... 而且这样是不太能处理子树问题的。

尝试口糊一个做法发现是假的。这题咕了。

## GP of Siberia

$$
dp[i,c]=dp[i-1,c], c \neq a_i\\
dp[i,c]=\sum dp[i-1,*]+1 ,c = a_i
$$

不会 DP of DP。先咕着。

## Balanced Sequence

> 有 n 个括号序列，你可以把它们排列之后首尾相连拼在一起，然后删去一些字符使得剩下的是合法括号序列。问最少删多少个字符。$\sum |S_i|\le 5\times 10^6$。

首先可以把每个串内的合法括号子序列（注意是子序列不是子串）删去，剩下形如 `))))(((((` 的序列，假设  `)`有 a 个，`(`有 b 个，表示为 $(a,b)$。考虑将二元组排序。首先 $a<b$ 的肯定在 $a>b$ 的前面。同为 $a<b$ 的显然 $a$ 小的在前面；同为 $a>b$ 的显然 $b$ 小的在后面。

这时顺着扫一遍前缀和，如果仍出现小于 0 的情况，就删。如果之前累积的`)`不够删就无解。另一种方法是考虑两个二元组的合并，可以发现
$$
(a,b)+(c,d)=(a+\max(0,b-c),d+\max(0,c-b))
$$
于是把相邻的合并完了比较最后的二元组的两者的大小就能得到答案。

## 打怪兽

> 打一个怪兽要先掉 $a_i$ 滴血再补 $b_i$ 滴血。树上每个点有个怪兽，打完一个点父亲才能打这个点，问最少要多少血使得血始终非负。

对于没有顺序的时候。先选一排序下最小的点。可以轻松证明，当能打这个点的时候，打这个点一定是最优的。

如果它是根，那么一定是最优的。否则当它的父节点被达到的时候，打它一定是最优的。调整法证明。

每次选顺序最小的。如果它的父亲是根就打他，不然就把它和父亲合并。即 (a,b)+(c,d)=(a+max(0,b-c),d+max(0,c-b))。因此变成了，找到当前最小值，和它的父亲合并缩点。缩点可以并查集维护。开一个堆维护每次找最小值的操作。

复杂度 nlogn。可以扩展到子树上。UOJ418。那就用树上可并数据结构维护一下。

HDUMonster Hunter

---

To be continued

## 异或

mod n 放下面

模 2 意义下的神奇多项式平方。把 T 二进制分解一下就能暴力求了。

## SRM733Hard

竞赛图：完全**有向**图。结论：有哈密尔顿回路当且仅当图是强连通的。

每个点都有入度和出度。

每次找一个点使得一个点指向它指向下一个相邻的点就可以暴力插入到路径中。

## CF868 F

转移时可以像莫队一样 O(1) 扩展两边。决策具有单调性，可以分治优化 DP。复杂度 O(nklogn)。

## 找子矩阵

好像又是一个对我来说很简单的分治优化 DP。

## CodeFestival16FinalH

如果中间有东西，那么左边的人肯定把中间的全吃掉。

每一轮左边的人走到右边的某一个位置，然后另一个人把之间的全部走完，然后走到右边相邻的位置（这样最优）。

f[i] 为只考虑第 i+1 到 n 只鸽子时第一个人的得分。
$$
f[i]=\max_{j=i+1}^n\left\{a_j-f_j-\sum_{k=i+1}^{j-1}a_k\right\}
$$
然后就掉线了......

https://cf16-final.contest.atcoder.jp/data/other/cf16-final/editorial.pdf

## UOJ445

掉线......

## CF908G

~~大概数位 DP 一下吧，f[i,j] 表示 i 位的，最大数字为 j 的数的和。~~。好像处理不了上界。

枚举第 i 位为 x 的方案数。然后做差分，转化为大于等于 x。差分后可以 DP。

可以组合数，固定前缀，枚举加入一个数 x，那么就要考虑大于 x 的数的个数，可以组合数算。

https://blog.csdn.net/wxh010910/article/details/78941592

## CF986C

额题读错了。

BFS 建补图？

## CF1012F

？？？

P=1 可以状圧

## 三元环计数

> 给一个无向图 G=(V,E)，求出这个图长度为 3 的环的个数。

考虑度数从小到大依次枚举结点。

## 度数分类

我们把它的结点分类考虑。结点 u 的度为 $d[u]$，则我们以 $\sqrt{m}$ 为分界。把 $d[u]\le \sqrt{m}$ 的点称作 A 类点，$d[u]>\sqrt{m}$ 的称作 B 类点。对于一个三元环，把包含 A 类点的称作 A 类环，不包含 A 类点的称作 B 类环。

先考虑 A 类环的统计。找到一个 A 类点 x，我们枚举与这个点相邻的两个结点 y,z，问题转化为判断 y,z 是否有连边。我们事先把所有的边存到一个 hashmap 里就可以做到 $O(1)$ 的判断。

然后考虑计数的次数。如果这个环是 AAA 型，则它被枚举了三次；如果是 ABB 型的就只被枚举了一次，于是把它的贡献补成 3（即把它当作三个环）；AAB 型的环则被枚举了两次，于是我们规定当当前枚举的 x 是编号较小的 A 就把它算 2 次贡献，否则就是一次贡献。这样就保证每个 A 类环都被算了 3 次，可以直接除以 3。

考虑一下这样做的复杂度。A 类点的度数不超过 $\sqrt{m}$，因为 m 条边最多每条边遍历一次，所以复杂度约为 $O(m\sqrt m)$.

然后考虑 B 类环的统计。由于 B 类点个数不超过 $\sqrt{m}$，于是暴力枚举 B 类点判断是否联通即可。由于 B 类点的个数不超过 $\sqrt m$，因此复杂度仍为 $O(m\sqrt m)$。

综上，总复杂度 $O(m\sqrt m)$，不过感觉不大稳就是了。

## 定向法

对所有的无向边定向，度数大的连度数小的，度数相同的就编号小的连标号大的。于是这个图就变成了一个 $DAG=(V,E')$。然后枚举每个点 u，把与 u 相邻的点打标记。然后做双重枚举，枚举与 u 相邻的点 v，再枚举与 v 相邻的点 w。如果 w 有标记就说明 (u,v,w) 是一个环。这样算下来可以保证每个环只被算了一次。

接下来考虑一下上述算法的正确性。首先它是一个 DAG 是显然的。每个环在定向后相当于只有一个入度为 0 的点，因此只被算一次。那么时间复杂度？

打标记操作会把每条边恰好遍历一次，复杂度 $O(n+m)$。

而在双重枚举的过程中对时间复杂度的贡献为
$$
\sum_{u\in V}\sum_{(u\to v)\in E'}d[v]
$$
对此分类讨论：

1. $d[v]=O(\sqrt m)$，因为 $(u\to v)\in E'$，则 $d[u]\ge d[v]$。这样的 u 有 $O(n)$ 个，总复杂度 $O(n\sqrt m)$。
2. $d[v]=\Omega(\sqrt m)=O(m)$，则有 $d[u]\ge d[v]\ge \sqrt m$，这样的 u 是 $O(\sqrt m)$ 个的，因此总复杂度为 $O(m\sqrt m)$.

于是总复杂度 $O(m\sqrt m)$。

## 四元环计数

> 给一个无向图 G=(V,E)，求出这个图长度为 4 的环的个数。

也可以有一个度数乱搞的做法，但这玩意儿要有一堆的分类讨论，非常开心。使用定向法的复杂度是假的。目前先咕着。

```
FOR x = 1 to N
	FOR y in G[x] //x -- y
		FOR z in D[y] //y -> z
			if rank[x]>rank[z]
				ans+=res[z]
				res[z]++
```

## LOJ3070

分治 NTT...... 直接跑路。
