---
title: 2019CSP-S 赛前冲刺 day8
date: 2019-10-25 15:13:17
tags:
  - Complete-Solution
---

## A

简单贪心题。考虑序列中一段连续上升，则这一段的重要度是一样的。于是这样可以得到每个点的重要度。然后再贪心地构造字典序最小的排列即可。

## B

似乎我的做法是最无脑的？

将两个笼子 $u,v$ 合并，我们建立一个新的点 $z$，左儿子连接 $u$，右儿子连 $v$。一个点要胜利相当于要走到这个树的个根结点。对于一个点 $u$，设 $f(u,x)$ 表示在点 $u$ 上胜利的种类是 $x$ 的方案数。初始时每个单点 $f(u,0)=f(u,1)=f(u,2)=1$，并且我们认为种类 $i$ 可以胜过 $(i+1)\bmod 3$。

则假设 $u$ 是主场，则
$$
f(z,0)=f(v,0)f(u,1)+f(u,0)(f(v,0)+f(v,1))\\
f(z,1)=f(v,1)f(u,2)+f(u,1)(f(v,1)+f(v,2))\\
f(z,2)=f(v,2)f(u,0)+f(u,2)(f(v,2)+f(v,0))
$$
考虑询问。我们分三类统计方案数，最后加起来。假设我们要求 $u$ 的分类是 $x$ 时要打赢它的兄弟结点 $v$ 的方案数。记一个状态 $k=1/0$ 表示 $u$ 是否是主场，则可以通过类似的转移算出答案。而你发现每次转移是一个乘法，于是我考场上写了一个带矩阵权并查集，最后半小时把暴力改成正解了。

后来发现，$f(u,0)=f(u,1)=f(u,2)$ 一定成立。于是似乎就更简单了。

复杂度 $O(n\log_2n)$。

## C

每次我们在 $T_{base}$ 的每个结点上挂一个 $T_{i-1}$，考虑求最大独立集的过程，注意到我们是不需要知道这个 $T_{i-1}$ 的具体结构的，只需要知道它的根结点是否在独立集中即可。

求最大独立集除了 DP，有一个贪心做法。考虑递归地**确定**最大独立集的方案。如果是叶结点就选这个点。对于一个内部结点 $u$，如果它的子节点都没有选，则选这个点 $u$。这样可以构造一个独立集方案，并且可以贪心证明它一定是最大独立集。而且我们是贪心地选择叶子结点，因此如果选择根结点和不选择根结点求出的最大独立集有相同的大小，那么我们的构造方法一定会导出不选择根结点的那个方案。

考虑 $T_{i-1}$ 按照上述方法构造的最大独立集是否选择根结点。如果不选择根结点，相当于在 $T_{base}$ 的每个结点下挂了一个没用的点，我们仍然可以用同样的方法求出 $T_{base}$ 的最大独立集，再加上 $n$ 个 $T_{i-1}$ 的最大独立集即可；否则，每个点下面都挂了一个被选择的点，则我们在 $T_{base}$ 中无法选择任何点，最大独立数就是 $n$ 个 $T_{i-1}$ 的最大独立数之和。

于是我们预处理出 $T(i)$ 的最大独立集方案中根结点 $i$ 是否被选（注意到换根并不影响最大独立集的大小但可能影响方案）。然后每次记录 $T_{i-1}$ 的根结点是否被选，并更新答案即可。

时间复杂度 $O(n)$。
