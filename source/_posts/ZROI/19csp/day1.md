---
title: 2019CSP-S 赛前冲刺 day1
date: 2019-10-14 14:03:26
tags:
  - Complete-Solution
---

## A

贪心题。

分两种情况：第一个位置放奇数和第一个位置放偶数。然后分奇数偶数，相当于你有若干个位置和若干个数，你要把这些数丢到这些位置上使得移动的距离和最小。

显然按次序匹配可以达到最小值。若两个匹配对应的区间是有交的，则交换他们的数，距离和不变。我们考虑这样调整来达到字典序最小值。

具体地，考虑匹配是向左还是向右的（匹配的数在匹配的位置的左边还是右边）。由于按序匹配是没有交叉的，因此向左和向右也是独立的，可以分开考虑。

对于向左的情况，我们维护一个小根堆，从左往右考虑位置，把每一个位置前的所有数加入堆中，然后取堆中最小的数作为这个位置上的数。

对于向右的情况同理，但我们维护的是大根堆，从右往左每次把最大的数放到这个位置上。

可以证明，这两种做法都可以找到最小的字典序。

考场上想的是数之间的交换，异常复杂。再加上身体状态不好，于是回光返照了……

遇到这种奇奇怪怪的贪心，一定要写暴力对拍！

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/1/a.cpp)

## B

一个森林的连通块个数即为点数减边数。在这题中就是亮着的灯泡数减去连续灯泡数。

分治。我们将颜色按照出现次数分类，约定一个阀值 $T$。维护亮着的灯泡数很容易。考虑维护连续灯泡数。

如果当前颜色出现次数小于 $T$，则我们枚举这个颜色的出现位置并更新连续灯泡数。

如果当前颜色出现次数大于等于 $T$，则这种颜色的个数是 $O\left(\frac{n}{T}\right)$ 的。分类讨论：
1. 相邻的是大颜色，则我们预处理大的颜色之间的连边与边数，这样就可以直接查询；
2. 相邻的是小颜色。我们放在小颜色来统计。具体地，对于每一个大颜色我们记录一个 tag 表示与它相邻的亮着的小颜色的灯的个数。显然在枚举小颜色的时候可以顺便维护。这样就完成这部分的维护了。

取 $T=\sqrt{n}$，时间复杂度 $O(n+q\sqrt{n})$。

最后提醒一点：根号复杂度一定要卡常！（详情见订正时的提交记录）

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/1/b.cpp)

## C

一道神奇的数学题。设 $q=1-p$。

设 $f_{n,k}$ 表示 $n$ 个人中选 $k$ 个人的概率。则考虑加一个编号 $n+1$ 的人进来，它要么加入选择的集合（赢其他所有人），要么不加入（这 $k$ 个人要赢他），则容易得到

$$
f_{n+1,k}=f_{n,k}\cdot p^{k}+f_{n,k-1}\cdot q^{n-k+1}
$$

我们再用同样的方法考虑加入一个编号为 $1$ 的人进来：

$$
f_{n+1,k}=f_{n,k}\cdot q^{k}+f_{n,k-1}\cdot p^{n-k+1}
$$

联立得到

$$
f_{n,k}(p^k-q^k)=f_{n,k-1}(p^{n-k+1}-q^{n-k+1})
$$

注意到 $f_{n,0}=1$。当 $p\ne q$ 时可以利用上式计算。当 $p=q=\dfrac{1}{2}$ 时，胜负概率相同。则可以组合数计算：

$$
f_{n,k}=\binom{n}{k}\frac{1}{2^{k(n-k)}}
$$

时间复杂度 $O(n)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/1/c.cpp)
