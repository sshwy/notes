---
title: 2019CSP-S 赛前冲刺 day10
date: 2019-10-29 14:57:26
tags:
  - Complete-Solution
---

## A

我们把上边界和下边界当作“点”，则考虑建立 MST，我们显然可以选择 MST 上最大的边作为直径，并且这是最大的答案。容易反证证明。

使用 Prim 算法，复杂度 $O(n^2+m)$。注意，一但上下边界连通就可以输出当前的答案了（即我们从上边界开始扩展）。

## B

考虑二分最大值，然后判断是否有解。

假设当前矩形是 $(x,y)$，则我们可以把点分 4 类：

1. 矩形内的点；
2. 矩形正上方的点；
3. 矩形正右方的点；
4. 矩形右上方的点。

一开始所有点都是第 4 类。在矩形的扩展中第 4 类的点可能会变成第 1,2,3 类的点。于是我们每次找到第 4 类点中距离 $(x,y)$ 最近点 $(x_1,y_1)$。由于 $x<x_1,y<y_1$，因此距离为 $x_1-x+y_1-y$，因此我们可以按照 $x_1+y_1$ 建立小根堆维护第 4 类点。每次从堆中取一个点：

1. 如果可以从 $(x,y)$ 走到 $(x_1,y_1)$ 就直接走过去，更新 $(x,y)$；
2. 如果这个点属于第 1,2,3 类点，就把它丢到对应的分类中；
3. 不可达这个点。

对于第 2,3 类点，显然可以使用两个堆分别维护，每次拿出一个点，检查是否可以扩展。

如果三个堆的点都不能扩展则无解。否则最后所有点都应该丢到第 1 类中。在扩展的过程中可以顺便记录方案。

时间复杂度 $O(n\log_2^2n)$。

然而考场上写了一个带 break 的 $O(n^2\log_2n)$，它也过了……

## C

网格图是二分图，容易想到将左脚和有脚匹配。即二分图的左部是左脚鞋子，右部是右脚鞋子。这样做一次二分图匹配。

如果最后有鞋子不在匹配中（不是完美匹配），我们就可以拿一个鞋子当工具鞋子，调整其他鞋子的方向使得一对匹配的鞋子总能让小 D 穿上。因此这种情况下的答案就是匹配数。

如果所有鞋子都在匹配中（完美匹配），我们给每个方向赋权，上下左右各为 0,1,2,3，则我们大胆猜想：答案为匹配数当前仅当初始状态的权值和与目标状态的权值和在模 4 意义下相等。否则答案为匹配数减 1。

**定理 1：**所有完美匹配的权值和对 4 取模的结果相同。

证明：考虑一个调整匹配的増广路，它一定形成了一个环（否则该匹配并非完美匹配）。考虑这个环导致的权值变化，可以发现是拐角处需要顺时针或逆时针旋转此处对应的内角那么多。因此，总的权值变化量就是内角和除以 $90^\circ$ 后的结果。注意到网格图中的环形必定具有偶数个角，因此该变化量一定是 4 的倍数。

**定理 2：**任意权值和对 4 取模后相同的状态是相互可达的。

证明：按照所有节点与 (1,1) 的曼哈顿距离分类，按照从右下到左上的顺序进行还原， 则我们每次可以通过上一条对角线还原下一条对角线上的方向，最终只剩下左上角的格子可能没有归位。因为权值对 4 取模的结果是一样的，所以这个格子也是与目标状 态相一致的，因此这样操作一定可以实现任意权值和对 4 取模后相同的状态之间的相 互转化。

证明都是贴的，考场上估计只能盲猜了。
