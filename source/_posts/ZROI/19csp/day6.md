---
title: 2019CSP-S 赛前冲刺 day6
date: 2019-10-23 14:13:50
tags:
  - Complete-Solution
---

## A

第一问可以随便做。单调栈、并查集、笛卡尔树都可以。

第二问，考虑构建出笛卡尔树（小根堆性质），即每次找到最小的元素，左右递归建树。

考虑我们从最小值到次小值搭梯子，如`xxxxx1xxxx2xxx`，那么我们搭梯的策略应该是，先从 $2$ 搭高度为 $2$ 的梯子，直到与 $1$ 相邻时下降一格。这样肯定不劣。

在笛卡尔树上相当于我们从当前结点向它的左右儿子搭梯子。这样我们递归搭梯，每次记录当前区间从最小值到左端搭的一段连续的桥的高度、到右端的连续的桥的高度（如果没有就记 -1）。这样，我们先判断是否需要搭梯。如果已经存在足够高的梯子就不用搭了，不然就要造新的桥。

因为是从低到高搭梯，可以保证贪心的正确性。

看 [代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/6/a.cpp) 可能更好懂。

## B

我们可以把大于 $k$ 的记为 1，等于 $k$ 的记为 0，小于 $k$ 的记为 -1。

我们考虑从 0 扩展，把相邻的都变成 0。

1. 对于`0 1 1`的情况，可以直接取一次 $\min$ 变成`0 0 1`；
2. 对于`0 1 -1`，我们就得先取一次 $\min$ 再取一次 $\max$ 变成`0 0 -1`。

于是推广上述规则，我们发现对于 1，-1 交替的情况，我们都需要一次额外的操作来修正。但仍有一种特殊情况，就是`0 1 -1 1`，我们可以只在中间取一次 $\max$ 得到`0 1 1 1`，而不用从左到右顺次修正。

综上所述，对于一段连续的`1 -1 1 -1...`其额外修正次数是段长除以 2 向下取整。

形式化地，如果两个相邻元素中有一个是 0，或者两个相邻元素相同， 就在中间放一个分割线。那么答案就是每一段的长度除以 2 向下取整，加上非零元素的个数。

在 $k$ 增大的时候会把一些 1 改成 0，把一些 0 改成 -1。用 set 维护分割线的位置即可。

复杂度 $O(n\log_2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/6/b.cpp)

## C

考虑先手什么时候必输。我们把树上的结点做子孙匹配，每个结点可以选择**子孙**中未匹配的点做匹配。假设我们求出了一个祖孙完备匹配（即每个点都找到匹配），那么先手走一个点，后手总可以走到匹配的点。如果先手走不了它就输了。于是这就是一个必败局面。

我们考虑统计所有必败态，然后用 $2^n$ 减掉即为答案。

考虑 DP。设 $f(i,j)$ 表示在子树 $i$ 内 ban 掉若干个点后，剩下的点贪心匹配，有 $j$ 个点没匹配的最大匹配的方案数（注意，这 $j$ 个点不包括被 ban 掉的点）。两个方案不同仅当 ban 掉的点不同，或者没匹配的点不同（即不在意匹配的方案，只在意哪些点匹配了）。

考虑结点 $i$ 个点有没有被 ban：

1. 如果被 ban，相当于在子树内选择总共 $j$ 个点不匹配；
2. 如果没被 ban，就考虑是否匹配。如果子树内不存在没匹配的点，则结点 $i$ 没法匹配；否则可以选择子树中的某个未匹配点匹配。

于是统计的时候我们先做一个背包，$g(j)$ 表示子树内一共选 $j$ 个的方案数：
$$
g(j)=\sum_{v_i\in Son(i),\sum k_i=j}\prod_{i=1}^{|Son(i)|}f(v_i,k_i)
$$
边界是 $g(0)=1$。这是个模板的树上背包，对子树大小取 $\min$ 可以做到 $O(n^2)$ 的复杂度。转移的时候：

1. 如果 $i$ 被 ban，相当于 $g(j)\to f(i,j)$。
2. 如果没被 ban，就考虑是否匹配。如果子树内不存在没匹配的点，相当于 $g(0)\to f(i,1)$；否则 $g(j+1)\to f(j)$。

这里的 $\to$ 表示贡献。

答案为 $2^n-f(1,0)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/6/c.cpp)

