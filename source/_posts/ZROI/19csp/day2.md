---
title: 2019CSP-S 赛前冲刺 day2
date: 2019-10-15 13:03:26
tags:
  - Complete-Solution
---

## A

一道套路期望题，见概率小结的 E6。

## B

直接枚举 $x$ 二分，复杂度 $O(nm\log_2n)$。

对于一个答案，二分 check 一次的复杂度是 $O(n)$ 的。而 $O(nm)$ 是可接受的复杂度，考虑乱搞。

原本我们是按序枚举 $x$，现在我们把顺序 random_shuffle 一下，则在期望情况下，新的答案比之前所有答案都小的个数的期望是 $\sum\dfrac{1}{i}=\Theta{\ln n}$，即调和级数。

于是我们随机枚举 x 后，先 check 一下 ans-1 是否合法，如果合法就二分，否则不二分。

时间复杂度 $O(nm+n\ln n\log_2n)$。

## C

考场做法是莫比乌斯反演。

定义 $f(x)$ 表示选择若干个两两不同**且互质**的数，LCM 为 x 的方案数。显然答案为 $f(n)$。

再定义 $F(x)$ 表示选择若干两两不同的数，LCM 为 x 的方案数。则

$$
\sum_{d|x}f(d)=F(x)
$$

相当于我们枚举这些数的 GCD 算贡献，就得到了上式。这显然可以莫比乌斯反演：

$$
f(x)=\sum_{d|x}F(d)\mu\left(\frac{x}{d}\right)
$$

我们再设 $G(x)$ 表示选择若干个两两不同的数，他们都是 x 的约数的方案数。则枚举 LCM 算贡献，可以得到

$$
\sum_{d|x}F(d)=G(x)\\
F(x)=\sum_{d|x}G(d)\mu\left(\frac{x}{d}\right)
$$

设 $d(x)$ 表示 $x$ 的约数个数，则显然 $G(x)=2^{d(x)}-1$。

然后两个莫比乌斯反演套在一起：

$$
f(x)\sum_{d|x}\mu\left(\frac{x}{d}\right)\sum_{p|d}G(p)\mu\left(\frac{d}{p}\right)\\
=\sum_{p|x}G(p)\sum_{d|\frac{x}{p}}\mu(d)\mu\left(\frac{x}{dp}\right)
$$

直接这么做似乎可以拿到 60 分。仔细观察发现，后面那个式子就是两个莫比乌斯函数的狄利克雷卷积：

$$
h(x)=\sum_{d|x}\mu(d)\mu\left(\frac{x}{d}\right)\\
f(x)\sum_{p|x}G(p)h\left(\frac{x}{p}\right)
$$

而 $h(x)$ 显然是一个积性函数，且

$$
h(1)=1\\
h(p)=-2\\
h(p^2)=1\\
h(p^k)=0(k\ge 3)
$$

其中 $p$ 是质数。

因此我们枚举 $n$ 的约数，记录每个约数的 $G(d),h(d)$，然后最后再统计答案即可。

枚举约数可以先质因数分解然后 DFS，可以使用 Pollard-Rho 算法。

时间复杂度 $O(n^{\frac{1}{4}}\log_2n+d(n)\log_2n)$。
