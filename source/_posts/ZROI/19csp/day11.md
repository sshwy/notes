---
title: 2019CSP-S 赛前冲刺 day11
date: 2019-10-30 20:12:05
tags:
  - Complete-Solution
---

## A

考虑枚举一个人是 Rk1，假设这个人是 a。然后考虑每一位：

对于 a 和 1 号都是`Y`的位，我们把这一位放前面，显然可以排掉这一位是`N`的选手（即不可能竞争 Rk2）。到目前为止 1 和 a 都是并列的 Rk1

上述情况都放完后，考虑 Rk1 为`Y`，1 号为`N`的位。如果存在某个这样的位使得只有 Rk1 一个人是`Y`，那么我们把这一位依次放到`YY`位的后面，这样可以保证 a 是绝对的 Rk1，而 1 只需要和剩下的人竞争剩下人中的 Rk1 即可。

如果找不到这个位，说明这个 a 不可能是 Rk1，或者 1 号不可能是 Rk2。

然后剩下的位就如法炮制，把 1 整成剩下人的 Rk1 即可。

康 [代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/11/a.cpp) 更好懂。

## B

考虑 DP。设 $f(i,j,0/1)$ 表示我们现在已经走完了 $[i,j]$，现在在左端点 / 右端点，并且我们把当前仓库里的方块拿在手上当作手上的方块，最少需要多少方块能走完所有仓库。

注意到如果你在左端点的位置，则 $[i,j]$ 内其他仓库的方块的个数必然是其左边的限制数。因为你从 $j$ 走到 $i$ 的时候会贪心地留下最少个数的方块。右端点同理。

考虑从 $i$ 走到 $i-1$，则我们要跨过 $h_{i-1}$ 的限制，因此 $f(i,j,0)\ge h_{i-1}$。跨过去后我们手里的方块数是 $f(i,j,0)-h_{i-1}+f_{i-1}$，则我们必须能走完所有仓库，因此 $f(i,j,0)-h_{i-1}+f_{i-1}\ge f(i-1,j,0)$。于是得到
$$
f(i,j,0)=h_{i-1}+\max(0,f(i-1,j,0)-f_{i-1})
$$
考虑从 $i$ 走到 $j+1$。可以理解为 $h_{i-1}$ 太大了，我们要先到右边去拿一些方块再回来跨过这个限制。仔细分析可以发现，从 $i$ 走到 $j+1$，用来跨过限制的方块数是不变的。对于一个 $[i,j]$ 中的限制 $h_k$，本来是它右边的仓库有 $h_k$ 个方块，现在变成了它左边的仓库有 $h_k$ 个方块。而要从 $i$ 走到 $j+1$，则要求手中的方块数大于等于这些限制中最大的那个，即 $f(i,j,0)\ge \displaystyle\max_{k=i}^j h_k$。而我们跨过 $h_j$ 只需要多耗费 $h_j$ 个方块，因此我们剩下的方块数是 $f(i,j,0)-h_j+f_{j+1}$，为了走完所有方块，我们要求 $f(i,j,0)-h_j+f_{j+1}\ge f(i,j+1,1)$。
$$
f(i,j,0)=\max\left(\max_{k=i}^j h_k,f(i,j+1,1)+h_j-f_{j+1}\right)
$$
两者取 $\min$ 即可得到 $f(i,j,0)$ 的转移式。$f(i,j,1)$ 同理。边界 $f(1,n,0/1)=0$，答案 $\max(f(i,i,0/1),0)$。

时间复杂度 $O(n^2)$。

## C

首先我们可以预处理出每个三元组在改 0,1,2,3 个数的情况下能够到达的最小的和与最大的和，即值域区间。记 $[l_{i,j},r_{i,j}]$ 表示第 $i$ 个三元组改 $j$ 个数的值域区间。

对于任意一个合法的方案，我们显然调整这个方案使得每个三元组的和等于某个区间的端点，因此可以离散化这些区间。设离散化后的最大权值为 $V$。

考虑 DP。$f(i,j)$ 表示前 $i$ 个三元组，其中第 $i$ 个三元组的和为 $j$，让他们非严格单减的最小代价。容易得到
$$
f(i,j)=\max_{k=j}^{V}f(i-1,k)+ \begin{cases}
0 & j\in[l_{i,0},r_{i,0}]\\
1 & j\in[l_{i,1},r_{i,1}]\\
2 & j\in[l_{i,2},r_{i,2}]\\
3 & j\in[l_{i,3},r_{i,3}]
\end{cases}
$$
直接转移，复杂度 $O(nV)=O(n^2)$。

事实上注意到 $[l_{i,0},r_{i,0}]\subseteq[l_{i,1},r_{i,1}]$，因此我们没必要用第 2 个转移来做第一种情况，因此：
$$
f(i,j)=\max_{k=j}^{V}f(i-1,k)+ \begin{cases}
0 & j\in[l_{i,0},r_{i,0}]\\
1 & j\in[l_{i,1},l_{i,0})\cup (r_{i,0},r_{i,1}]\\
2 & j\in[l_{i,2},l_{i,1})\cup (r_{i,1},r_{i,2}]\\
3 & j\in[l_{i,3},l_{i,2})\cup (r_{i,2},r_{i,3}]\\
\end{cases}
$$
这 7 个区间是互不相交的，可以分别处理。考虑使用线段树优化转移。线段树第 $j$ 位上维护 $f(i,j)$。考虑如何把线段树从 $f(i-1)$ 更新到 $f(i)$。注意到上述转移其实就是对区间内的位置先取**各自**的后缀 $\min$，然后再做若干次区间加的操作。最终答案则是全局最小值。也就是说我们希望实现：

1. 区间 $[l,r]$ 内的数都加 $v$；
2. 区间 $[l,r]$ 内的数都变成各自的后缀最小值，即 $\forall j\in[l,r],A_j\gets \displaystyle\min_{k=j}^VA_k$。
3. 查询全局最小值。

为此我们可以通过区间查询最小值，区间对某个数取 $\min$ 来实现区间变成各自的最小值，思路就是向 CDQ 分治一样算右边对左边的贡献，然后打一个标记即可。

具体地，考虑我们将 $[l,r]$ 内的数取各自的后缀最小值，则首先将 $[l,r]$ 打一个对 $[r+1,V]$ 的最小值取 $\min$ 的标记。将 $[l,r]$ 拆分成 log 个区间后，我们倒序遍历这些区间，把在区间外的贡献全部打成对某个数取 $\min$ 的标记。这样就只剩下区间内的贡献，即我们给每个区间打一个对区间内的后缀取 $\min$ 的标记。下传的时候，考虑右子树对左子树的贡献，于是把右子树的最小值拿出来给左子树打一个取 $\min$ 的标记即可。

其实实现的时候没有这么麻烦，因为实际上你每次是全体对后缀取 $\min$，可以省掉一些部分。

注意标记之间互相影响的关系。

复杂度 $O(n\log_2n)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/11/c.cpp)

