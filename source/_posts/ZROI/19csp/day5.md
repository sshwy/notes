---
title: 2019CSP-S 赛前冲刺 day5
date: 2019-10-22 14:08:49
tags:
  - Complete-Solution
---

## A

如果剩了一个环，显然就无法变黑。因此不能有环。于是一个连通块留一个生成树即可。并查集实现（自环是不能自销的）

## B

设对于当前询问，$T=\{n_1,n_2,\cdots,n_k\}$。设 $S(a,b)=|\{x^a\le b\mid x\in Z^+\}|$

首先考虑二分答案，然后我们需要通过容斥来去重。直观来说，我们计算了 $S(a,x),S(b,x)$ 的和，就要减掉 $S([a,b])$。于是我们考虑计算每一个 $S(t,x)$ 的容斥系数 $g(t)$。这样可以求出小于等于 $x$ 的数的个数是 $\sum g(t)S(t,x)$。

考虑如何求容斥系数，本质上

$$
g(t)=\sum_{P\subseteq T}(-1)^{|P|-1}[\operatorname{lcm}(P)=t]
$$

我们记 $g(i,j)$ 表示

$$
g(i,j)=\sum_{P\subseteq \{n_1,n_2,\cdots,n_i\}}(-1)^{|P|-1}[\operatorname{lcm}(P)=j]
$$

也就是前 $i$ 项对 $j$ 的容斥系数。得到转移如下：

$$
g(i-1,j)\to g(i,j)\\
-g(i-1,j)\to g(i,\operatorname{lcm}(j,n_i))\\
[j=n_i] \to g(i,j)
$$

这里的箭头表示贡献。于是 $g(t)=g(k,t)$。

然后考虑求 $S(a,b)$，这个东西可以直接调用`cmath`的`pow`，然后加加减减调整一下精度即可。注意在快速幂检验的时候，如果快速幂的过程中超过了 $10^{17}$ 可以直接返回 $10^{17}+1$，不然可能乘爆 LL。

最后有一个细节，就是你发现 $1^t$ 对 $S(t,a)$ 总是有贡献。因此我们其实求的是 $S'(a,b)=S(a,b)-1$，把 $1$ 的贡献减掉。因为 $2^{60}>10^{17}$，因此 $S'(a,b)=0(a\ge 60)$，这样在计算容斥系数的时候，$\operatorname{lcm}> 60$ 就直接 break，容斥的时候也只用枚举到 $60$。最后算完了再加 1，就得到了最终的个数。

设 $M=10^{17}$，时间复杂度 $O(q(k\log_2M+\log_2^3M))$。

## C

很裸的 FWT 的题。要处理细节的问题。设 $c(x)$ 表示 $x$ 出现的次数。

1. 对于 and 的情况，自己 and 自己被算了两次，要减掉；
2. or 的情况，自己 or 自己被算了两次，要减掉；
3. xor 的情况，$x\oplus x=0$ 被算成了 $c(x)^2$，要改成 $c(x)(c(x)-1)$ 最后再除以 2。

上述问题，考场上写一个暴力对拍就可以发现了。

考场上写的时候 2 次变换写成了 3 次，成功被卡常成 75 分……赛后交就过了。但是改成 2 次变换更快，也不会被卡常。
