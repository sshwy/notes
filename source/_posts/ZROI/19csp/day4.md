---
title: 2019CSP-S 赛前冲刺 day4
date: 2019-10-21 15:39:36
tags:
  - Complete-Solution
---

## A

一道构造题，先说一下考场思路。

设根结点为 $1$，并且设 $S(i)$ 表示 $i$ 的第一个儿子结点。则 $f(i)$ 表示子树 $i$ 的 $i$ 到 $S(i)$ 的一个哈密尔顿通路方案。

于是得到

$$
f(u)=R\left(\left(\sum_{v\in Son(u)}f(v)\right) + u\right)
$$

这里的加法指方案序列的拼接，$R(S)$ 表示把序列 $S$ 翻转。

写一个带 tag 的 dfs 即可输出方案。

正解有一种站在出题人角度的思路的味道，直接分奇偶考虑，很简洁。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/4/a.cpp)

## B

把字符串处理成区间的形式。问题可以转化为，选择若干个元素覆盖所有区间且元素和最小。

包含的区间显然可以去掉，因此去重后排序，得到的区间序列满足左右端点均非降。

考虑 DP。设 $f(i)$ 表示覆盖与 $[1,i]$ 有交的区间的最小代价。则我们设 $[l_a,r_a]$ 表示与 $[i,i]$ 有交的最靠前的区间，$[l_b,r_b]$ 表示最靠后的区间（可能不存在）。则考虑 $a_i$ 选不选，可以得到转移式

$$
f(i)=\begin{cases}
\displaystyle\max_{k=l_a-1}^{i-1} f(k) + \displaystyle\min_{p=l_b}^i a_p & \text{a,b exists}\\
f(i-1) & \text{Otherwise}
\end{cases}
$$

线段树（单调队列）优化转移即可。复杂度 $O(nm+n\log_2n)$ 或者 $O(nm)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/4/b.cpp)

## C

观察发现，端点可以分开求。问题转化为在子树 $u$ 内找 $k$ 个两两 LCA 为 $u$ 的点的方案数（不定根，点可重叠）。显然可以枚举，有多少个点不是 $u$（在 $u$ 的子树中）。然后计算方案后乘一个组合数和阶乘上去。

但是注意到这是不定根的。假设 $u$ 是根结点，上述方案数可以用生成函数理解：

$$
f(x)=\prod_{u,v\in E}(1+S_v)
$$

这里的 $S_u$ 表示 $u$ 的子树大小。则选 $k$ 个不是 $u$ 的方案数就是 $[x^k]f(x)$。如果定了某个非 $u$ 的点为根，相当于要除掉一项。你发现这就是你算 $f(x)$ 的逆过程！

举个例子，设 $F(k)=[x^k]f(x)$，现在 $f'(x)=f(x)(1+a)$，则 $F'(k)=[x^k]f'(x)$，我们把它当成一个背包的问题，建立 $F(x)$ 到 $F'(x)$ 的转移式：

$$
F'(k)=F(k)+F(k-1)a
$$

则除掉一个 $(1+a)$，相当于你知道 $F'(k)$，你要求 $F(k)$：

$$
F(k)=F'(k)-a\cdot F(k-1)\\
F(0)=1
$$

于是从小到大求 $F(i)$ 即可。这就和缺 1 背包是相似的。

这样，用倍增找到根的方向，然后除掉对应的子树。总复杂度 $O((n+q)L+q(k+log_2n))$，可能要对点的度取个 $\min$ 统计，卡卡过。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/19csp-spt/4/c.cpp)
