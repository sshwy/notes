---
title: 省选特训 9 题解
date: 2020-04-07 20:26:00
tag:
---

## A 简单

> 定义形如 $SS$ 的串为偶串，给定一个偶串 $S$ ，每次在 $S$ 后加上尽量少的字符使得其仍是偶串。问 $\infty$ 次操作之后，$l$ 位置至 $r$ 位置之间每种字符的出现次数。
>
> $|S|\le 2\times 10^5,l\le r\le 10^{18}$。

{%label @字符串 %} {%label @周期定理 %}

考虑在 $SS$ 后面加了 $2k$ 个字符：

![p1](../../../images/sp9-1.png)

那么由于新的串是一个偶串，则可以表示为 $STST$ 的形式。容易发现 $T$ 一定是 $S$ 的一个周期（必要条件）。

那么这个条件足够充分吗？是否对于 $S$ 的任意一个周期 $T$，都可以让 $SS$ 是 $STST$ 的前缀？显然是的。

因此这是充要条件。换言之题面中的操作等价于，我们在 $S$ 后面加一个尽量短的 $S$ 的周期 $T$，让 $SS$ 变成 $STST$。也就是最小周期 $\text{per}(S)$。那么我们只考虑半边。也就是说每次的操作是，让 $S$ 变成 $ST$。

若 $T$ 是 $S$ 的整周期（即 $|T|\mid |S|$），则易证 $ST$ 的最小周期也是 $T$。因此 $\infty$ 次操作的结果就是 $ST^{\infty}$。可以快速统计答案。

否则。考虑 $ST$ 的最小周期是什么。我们断言，$\text{per}(ST)=S$。证明如下：

首先，$S$ 显然是 $ST$ 的周期。另外，$T$ 一定不是 $ST$ 的周期（因为它不是 $S$ 的整周期）。

假设 $ST$ 的最小周期是 $S'$（$|S'|<|S|$）。那么 $S'$ 显然也是 $S$ 的周期。并且 $|T|\nmid |S'|$，$|T|\le |S'|$。

根据**周期定理**，若 $|T|+|S'|-\gcd(|T|,|S'|)\le |S|$，那么 $\gcd(|T|,|S'|)$ 就是 $S$ 的周期。而这与 $T$ 是 $S$ 的最小周期矛盾。因此 $|S'|> |S| + \gcd(|T|,|S'|)-|T| > |S|-|T|$。

这里补充一个小**引理**：若串 $T$ 存在一个 border$B$ 使得 $B$ 同时也是 $T$ 的周期，则 $T$ 存在整周期。

证明：$\gcd(|B|,|T|-|B|) \mid T$。

由于 $T$ 是 $S$ 和 $S'$ 的周期。且 $|S|-|S'|<|T|$，因此 $ST$ 和 $S'T$ 两者的 $T$ 是有重叠部分的。这时可以发现 $T$ 出现了既是 border 又是周期的前缀。则与 $T$ 是最小周期矛盾。

证毕。

因此在这种情况下，$\text{per}(ST)=S$。那么使用数学归纳法。设 $g=S,g^2=ST$。则 $g^i=g^{i-1}+g^{i-2}$。则 $\infty$ 次操作后的结果就是 $g^{\infty}$。这部分的答案可以递归计算。

因此时间复杂度 $O(|S|+|\Sigma|\log_2r)$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/sp9/a.cpp)

## B 连赢

> 在 $n$ 个点的边带权树上有 $m$ 个点对 $(a_i , b_i)$ ，求
> $$
> \max_{1\le i,j\le m}\{\text{dist}(a_i,a_j)+\text{dist}(b_i,b_j)\}
> $$
> $n\le 2\times 10^5,m\le 10^5$。

{%label @点分治 %} {%label @直径合并 %}

题目给了这样一个部分分：所有 $a_i$ 都相等。我们来考虑这个部分怎么做。

容易发现，答案即为 $b$ 中的直径。

考虑把 $a$ 的距离拆开，得到 $\text{dist}(b_i,b_j)+\text{dep}(a_i)+\text{dep}(a_j)-2\text{dep}(\text{lca}(a_i,a_j))$。$\text{dist}(b_i,b_j)+\text{dep}(a_i)+\text{dep}(a_j)$ 可以看作是 $b$ 的端点带权直径。它仍满足普通直径的性质。你可以理解为是 $b_i$ 下面挂了一条长度为 $\text{dep}(a_i)$ 的边。这样它就可以当作普通的边带权直径看待。也支持直径合并等等的操作。

我们枚举 $\text{lca}(a_i,a_j)$。则问题转化为，统计 $a_i,a_j$ 在不同子树的，$\text{dist}(b_i,b_j)+\text{dep}(a_i)+\text{dep}(a_j)$ 的最大值。那么我们考虑每个子树维护子树内的 $a$ 对应的 $b$ 的集合的（端点带权）直径。那么我们在子树直径合并的时候有 $6$ 种更新直径的方式。其中的 $4$ 种是满足端点的 $a$ 在不同子树的，可以更新答案。剩余两种不能用于更新答案。这样在求直径的同时更新答案即可。

时间复杂度 $n\log_2n+m$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/sp9/b.cpp)

## C AK

> 定义复数 $a+bi$ 是整数 $k$ 的约数，当且仅当存在 $a$ 和 $b$ 为整数且存在整数 $c, d$ 满足 $(a+bi)(c+di)=k$。
>
> 求
> $$
> \sum_{i=1}^n\sum_{z\in C,z\mid i,\Re(z)>0}\Re(z)
> $$
> 其中 $\Re(z)=\text{Re}(z)$，表示 $z$ 的实部。

{%label @莫比乌斯反演 %} {%label @数论分块 %} {%label @卡常 %}

考场上做的时候，把 $x^2+y^2$ 的形式丢到分母去了，导致不太能做。
$$
\begin{aligned}
\sum_{i=1}^n\sum_{z|i,\Re(z)>0}\Re(z) &=\sum_{a}a\sum_b\sum_c\sum_d[1\le ac-bd\le n][ad+bc=0]\\
&= \sum_{a}a\sum_b\sum_c\sum_d[1\le ac-bd\le n]\left[\frac{a}{c}=-\frac{b}{d}\right]\\
&= \sum_{x}\sum_{a}\sum_{c}[a\perp c]ax\sum_y[1\le acx^2+acy^2\le n] \\
&= \sum_{i=1}^ni\sum_{j=1}^n[i\perp j]\sum_{x=1}^nx\sum_{y=-n}^n\left[1\le (x^2+y^2)\le \left\lfloor\frac{n}{ij}\right\rfloor \right] \\
&= \sum_{d=1}^n \mu(d)d \sum_{i=1}^{\lfloor\frac{n}{d}\rfloor}i\sum_{j=1}^{\lfloor\frac{n}{d}\rfloor} \sum_{x=1}^nx\sum_{y=-n}^n\left[1\le (x^2+y^2)\le \left\lfloor\frac{n}{ijd^2}\right\rfloor \right] \\
\end{aligned}
$$

不妨设 $f(n)=\sum_{x=1}^nx\sum_{y=-n}^n[1\le (x^2+y^2)\le n]$。
$$
\begin{aligned}
\sum_{i=1}^n\sum_{z|i}Re[z] &= \sum_{d=1}^n \mu(d)d \sum_{i=1}^{\lfloor\frac{n}{d}\rfloor}i\sum_{j=1}^{\lfloor\frac{n}{d}\rfloor} f\left(\left\lfloor\frac{n}{ijd^2}\right\rfloor \right) \\
&= \sum_{d=1}^n \mu(d)d \sum_{T=1}^{\lfloor\frac{n}{d^2}\rfloor}\sum_{i|T}i\cdot f\left(\left\lfloor\frac{n}{Td^2}\right\rfloor \right) \\
&= \sum_{d=1}^{\lfloor\sqrt{n}\rfloor} \mu(d)d \sum_{T=1}^{\lfloor\frac{n}{d^2}\rfloor}\sigma(T) f\left(\left\lfloor\frac{n}{Td^2}\right\rfloor \right) \\
\end{aligned}
$$
考虑化简 $f(n)$：
$$
\begin{aligned}
f(n)=\sum_{x=1}^{\lfloor\sqrt{n}\rfloor}x \left(2\left\lfloor\sqrt{n-x^2}\right\rfloor+1\right)
\end{aligned}
$$
预处理 $f(1),\cdots,f(m)$：相当于是半径为 $\sqrt{m}$ 的圆内的点，则总的点数是 $O(m)$ 的。直接枚举统计即可。

单点计算 $f(n)$：按照上式计算即可。

设 $s_\sigma$ 是 $\sigma$ 的前缀和。

预处理 $s_\sigma(1),\cdots,s_\sigma(m)$ ：先预处理 $\sigma(1),\cdots,\sigma(m)$ 然后前缀和即可。预处理可以线性筛。

单点计算 $s_\sigma(m)$ ：$s_\sigma(m)=\sum_{i=1}^m\sigma(i)=\sum_{i=1}^m\lfloor\frac{m}{i}\rfloor i$。可以数论分块。

预处理前 $n^{\frac{2}{3}}$ 的部分。总复杂度 $O(n^{\frac{2}{3}})$。

似乎卡常？

需要卡常。并且Get到一个写数论分块的姿势。详见代码里的`sg`函数。大致思路就是把前根号块和后根号块分开处理。减少一半的除法次数。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/sp9/c.cpp)
