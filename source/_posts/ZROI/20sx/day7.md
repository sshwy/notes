---
title: 2020 省选十连测 day7 & 附加赛
date: 2020-01-28 23:01:00
tag:
---

## 考场经历

写了一个半小时的T2分块，然后听说是原题就跑路了……

## A

> 定义二维平面上一个区域的凸包是这个区域里所有的点的凸包（即无限个点的凸包，可以理解为是顶/切点连线）。给出$n$个圆，问这$n$个圆的凸包的周长。
> 
> $n\le 100,1\le r_i\le 1000,|x_i|,|y_i|\le 10^3$。

**摘要：发现可以用顶点求凸包周长，转化为求顶点集合。转化为求外公切点集合的凸包。随机旋转来卡精度。**

首先，凸包的周长一定由若干段圆弧以及若干段线段构成。

思考发现，线段一定是切线段，并且一定是两个圆的外公切线上的线段。由于是外公切线，因此每个切点一定只属于一个圆。从这里也可以发现，凸包的顶点一定是圆上的切点。

对于一个凸包，如果我们知道它的顶点（切点）以及每个顶点所属的圆，那么我们就可以快速求出周长。如果两个相邻点属于同一个圆，那么这一段周长就是一段弧，否则就是这两个点的连线段。

而凸包的顶点一定是切点，则我们求出所有的外公切线的切点，然后对这些点求凸包。这样做完了吗？事实上，一个切点要是凸包的顶点，它不能被任何一个圆包含在内。加上这个条件后得到的点集才是凸包顶点集。然后我们就可以算出周长了。

时间复杂度$O(Tn^3)$。

计算几何题尤其是与圆有关的题，其精度都容易崩，这时一个比较好的方法是把所有图形旋转一个随机的角度，这样在计算斜率之类的时候会减小误差。

另外，`atan2(y,x)`计算的是以$x$轴非负半轴为始边，以向量$(x,y)$为终边的角的弧度，返回值的范围是$[-\pi,\pi]$。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/7/a.cpp)

## 附加赛

写了6k的T3结果被卡常，然后写了T2，没写T1，可能是当时短路了。

## 附加赛 A

> 给出一个01串集合$S=\{s_i|s_i[j]\in\{0,1\}\}$，定义这个集合的**权值**为它构成的Trie的结点个数。
>
> 现在给你一个字符集为`01?`的串的集合$S=\{s_i|s_i[j]\in\{0,1,?\}\}$，`?`只能被替换为`0`或`1`。假设一共有$k$个`?`，那么就一共有$2^k$种集合$S$，问$2^k$种情况的权值和。
>
> $|S|\le 20,|s_i|\le 50$。

一个串的权值容易统计。如果直接把两个串的权值加起来，那么两个串的公共前缀部分就会被重复统计。因此想到容斥。

容斥时设$f(T),T\subseteq S$表示在$2^k$种情况中$T$中的字符串的公共前缀长度和。显然答案为$\sum_{T\subseteq S}f(T)^{|T|-1}$。

然后设几个辅助变量算一下，把复杂度做到$O(2^n|s|)$就行了。

要卡常（全`?`）。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/7.5/a.cpp)

## 附加赛 C

> 对于数列$A$，定义其权值为它本质不同的子序列的个数。现在给出一个长度为$n$的序列$A$和一个整数$K$，有$m$次操作：
>
> 1. $\forall i\in[l,r],A_i\gets(A_i+x)\bmod K$。
> 2. $\forall i\in[l,r],A_i\gets(A_i\times x)\bmod K$。
> 3. 询问区间$[l,r]$形成的序列的权值。
>
> $n,m\le 30000,K\le 5,A_i,x\in[0,K)$。

**摘要：矩乘，置换，均摊重构。**

本质不同子序列个数有一个$O(n|\Sigma|^3)$的矩乘做法，可以用线段树优化。对于修改操作，加法就是简单的下标置换，而乘法要讨论一下，如果是置换的话就置换，否则就直接重构这一段区间即可。

时间复杂度$O(n|\Sigma|^3\log_2n)$。

接下来是卡常指南：

1. 快速IO；
2. 不要做无意义的初始化，如矩阵初始化为单位矩阵，置换初始化为单位置换；
3. 减少取模，尤其是矩乘；
4. 减少矩乘运算的调用；
5. 删assert。

[代码](https://gitee.com/sshwy/code/raw/master/zroi/20sx/7.5/c.cpp)