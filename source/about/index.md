---
title: About
date: 2019-10-13 14:02:29
comments: false
toc: false
---

本站已经成功从一个笔记站点转型为Sshwy的第二主站。当然，仍是是不便公开的。**请不要添加本博客的友链。**如果非常需要，你可以添加主站友链。

在2020年1月的时候，本博客主题更新为Ssimple，这是我亲手为本站开发的主题。

这个主题并不是偷懒，而是我特别设计的极简主题。

在OI圈混了一两年了，更博也更了一年了，算是半个自媒体从事者了。我越发感叹，当今OI圈的博客内容是如此雷同，我写博客的意义是什么。

是一种形式的日记，自己努力的证明，或者各种各样的原因。总之我在一段时间内还不会放弃更博。

那些与我交好的朋友，或者有心人，才会来看我的博客吧。既然如此，何必把博客弄得那么精致呢？这个主题，可能也是我从浮躁渐渐沉静下来的体现。愿意来阅读的人，自会用心。无心阅读的人，那么再浮华的样式也无济。

新的一年，给自己一个新的开始。你好，庚子！

*2020.1.12*

想了解关于博主过去的信息，请移步 [主站](https://sshwy.name)。

## Vim 配置文件

*2020.4.18*

```vim
" Basic Setting
syntax on
set nocp nu cin autoindent ts=4 sw=4 mouse=a smartindent smarttab
set backspace=indent,eol,start 
set foldmethod=marker foldlevel=99
autocmd FileType cpp set foldlevel=0
set encoding=utf-8 ambiwidth=double 
set et  " 编辑时将所有 Tab 替换为空格
set fo+=mB 
set selection=inclusive 
set wildmenu 
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1

imap jj <ESC>
nmap <F5> :!clear;g++ % -O2 -ggdb -o %< -Wall <ESC>
nmap <F6> :!clear;time ./%< <ESC>
nmap <F7> :!clear;time ./%< < %<.in <ESC>
nmap <F8> :vsp %<.in <ESC>
nmap <F9> :!clear;gdb --tui %< <ESC>
nmap <C-g> gg"+yG <ESC>
vmap <C-c> "+y <ESC>

set shell=/bin/zsh
au BufRead,BufNewFile *.ejs set filetype=html " 文件类型映射

"hi Conceal ctermfg=NONE
"hi Conceal guifg=NONE
hi Conceal ctermbg=NONE
hi Conceal guibg=NONE

function Copy_file_to_cplibroad()
    let @+=expand('%:p')
endfunction

nmap <C-f> :call Copy_file_to_cplibroad()<cr>

" Gvim Setting

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
```

